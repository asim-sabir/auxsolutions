<?php

use App\Models\Admin\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();
        Admin::create([
            'name' => 'admin',
            'email' => 'info@aux.com',
            'password' => bcrypt('aux@@')
        ]);
    }
}
