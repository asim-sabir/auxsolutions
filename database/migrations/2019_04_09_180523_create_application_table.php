<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->string('id', 25)->primary();
            $table->string('user_id', 25)->index();
            //            questionnaire
            $table->text('questionnaire_business_name')->nullable();
            $table->text('questionnaire_merchant_dba')->nullable();
            $table->text('questionnaire_dba_address')->nullable();
            $table->text('questionnaire_dba_city')->nullable();
            $table->text('questionnaire_dba_state')->nullable();
            $table->text('questionnaire_dba_zip')->nullable();
            $table->text('questionnaire_business_phone_no')->nullable();
            $table->text('questionnaire_total_business_months')->nullable();
            $table->text('questionnaire_email')->nullable();
            $table->text('questionnaire_annual_sales')->nullable();
            $table->text('questionnaire_tax_id')->nullable();
            $table->text('questionnaire_ownership_type')->nullable();
            $table->text('questionnaire_legal_address')->nullable();
            $table->text('questionnaire_legal_city')->nullable();
            $table->text('questionnaire_legal_state')->nullable();
            $table->text('questionnaire_legal_zip')->nullable();
            //            questionnaire Owner 1
            $table->text('questionnaire_owner_1_name')->nullable();
            $table->text('questionnaire_owner_1_address')->nullable();
            $table->text('questionnaire_owner_1_state')->nullable();
            $table->text('questionnaire_owner_1_city')->nullable();
            $table->text('questionnaire_owner_1_zip')->nullable();
            $table->text('questionnaire_owner_1_ownership_percentage')->nullable();
            $table->date('questionnaire_owner_1_birthday')->nullable();
            $table->text('questionnaire_owner_1_social_security')->nullable();
           //            questionnaire Owner 2
            $table->text('questionnaire_owner_2_name')->nullable();
            $table->text('questionnaire_owner_2_address')->nullable();
            $table->text('questionnaire_owner_2_state')->nullable();
            $table->text('questionnaire_owner_2_city')->nullable();
            $table->text('questionnaire_owner_2_zip')->nullable();
            $table->text('questionnaire_owner_2_ownership_percentage')->nullable();
            $table->date('questionnaire_owner_2_birthday')->nullable();
            $table->text('questionnaire_owner_2_social_security')->nullable();
            $table->text('questionnaire_has_previous_application')->nullable();
            $table->text('merchant_licence')->nullable();

            $table->text('corporate_name')->nullable();
            $table->text('corporate_address')->nullable();
            $table->text('corporate_city')->nullable();
            $table->text('corporate_state')->nullable();
            $table->text('corporate_zip')->nullable();
            $table->text('corporate_phone_no')->nullable();
            $table->text('corporate_fax_no')->nullable();
            $table->text('corporate_federal_tex_id')->nullable();
            $table->text('corporate_contact_person')->nullable();
            $table->text('doing_business_as')->nullable();
            $table->text('business_address')->nullable();
            $table->text('business_city')->nullable();
            $table->text('business_state')->nullable();
            $table->text('business_zip')->nullable();
            $table->text('business_phone_no_1')->nullable();
            $table->text('business_phone_no_2')->nullable();
            $table->text('business_email')->nullable();
            $table->text('type_of_entity')->nullable();
            $table->integer('firm_type')->nullable();
            $table->text('type_of_goods_sold')->nullable();
            $table->text('web_address')->nullable();
            $table->text('length_of_ownership_yrs')->nullable();
            $table->text('length_of_ownership_mos')->nullable();
            $table->text('length_of_time_at_location_yrs')->nullable();
            $table->text('length_of_time_at_location_mos')->nullable();
            $table->text('year_business_established')->nullable();
            $table->integer('is_licensed_medical_cannabis')->nullable();
            $table->text('is_licensed_medical_cannabis_detail')->nullable();
            $table->integer('date_of_original_licence_granted')->nullable();
            $table->text('date_of_original_licence_granted_detail')->nullable();
            $table->integer('license_no')->nullable();
            $table->text('license_no_detail')->nullable();
            $table->integer('license_expiration_date')->nullable();
            $table->text('license_expiration_date_detail')->nullable();
            $table->integer('licence_terminated')->nullable();
            $table->text('licence_terminated_detail')->nullable();
            $table->text('name_1')->nullable();
            $table->text('title_1')->nullable();
            $table->text('residential_address_1')->nullable();
            $table->text('residential_city_1')->nullable();
            $table->text('residential_state_1')->nullable();
            $table->text('residential_zip_1')->nullable();
            $table->text('driver_license_number_1')->nullable();
            $table->text('ssn_1')->nullable();
            $table->text('equity_ownership_1')->nullable();
            $table->text('time_at_residence_1_yrs')->nullable();
            $table->text('time_at_residence_1_mos')->nullable();
            $table->integer('residence_type_own_1')->nullable();
            $table->integer('residence_type_rent_1')->nullable();
            $table->date('date_of_birth_1')->nullable();
            $table->text('residence_telephone_1')->nullable();
            $table->text('name_2')->nullable();
            $table->text('title_2')->nullable();
            $table->text('residential_address_2')->nullable();
            $table->text('residential_city_2')->nullable();
            $table->text('residential_state_2')->nullable();
            $table->text('residential_zip_2')->nullable();
            $table->text('driver_license_number_2')->nullable();
            $table->text('ssn_2')->nullable();
            $table->text('equity_ownership_2')->nullable();
            $table->text('time_at_residence_2_yrs')->nullable();
            $table->text('time_at_residence_2_mos')->nullable();
            $table->integer('residence_type_own_2')->nullable();
            $table->integer('residence_type_rent_2')->nullable();
            $table->date('date_of_birth_2')->nullable();
            $table->text('residence_telephone_2')->nullable();
            $table->text('bank_reference')->nullable();
            $table->text('bank_reference_account_no')->nullable();
            $table->text('bank_reference_telephone_no')->nullable();
            $table->text('bank_reference_contact')->nullable();
            $table->text('trade_reference_1')->nullable();
            $table->text('trade_reference_1_account_no')->nullable();
            $table->text('trade_reference_1_telephone_no')->nullable();
            $table->text('trade_reference_1_contact')->nullable();
            $table->text('trade_reference_2')->nullable();
            $table->text('trade_reference_2_account_no')->nullable();
            $table->text('trade_reference_2_telephone_no')->nullable();
            $table->text('trade_reference_2_contact')->nullable();
            $table->text('trade_reference_3')->nullable();
            $table->text('trade_reference_3_account_no')->nullable();
            $table->text('trade_reference_3_telephone_no')->nullable();
            $table->text('trade_reference_3_contact')->nullable();
            $table->text('form_w9_name')->nullable();
            $table->text('form_w9_business_name')->nullable();
            $table->text('form_w9_address')->nullable();
            $table->text('form_w9_city')->nullable();
            $table->text('form_w9_state')->nullable();
            $table->text('form_w9_zip_code')->nullable();
            $table->text('form_w9_requester_name')->nullable();
            $table->text('form_w9_requester_address')->nullable();
            $table->text('form_w9_account_no_list')->nullable();
            $table->text('social_security_number')->nullable();
            $table->text('employer_identification_number')->nullable();
            $table->tinyInteger('fee_schedule_status')->default(1)->comment('1: active 2: inactive');
            $table->tinyInteger('one_time_equipment_set_up')->nullable();
            $table->text('one_time_equipment_set_up_amount')->nullable();
            $table->text('additional_merchant_terminal_amount')->nullable();
            $table->tinyInteger('sale_volume_fee')->nullable();
            $table->text('sale_volume_fee_amount')->nullable();
            $table->text('per_transaction_amount')->nullable();
            $table->tinyInteger('charge_back')->nullable();
            $table->text('charge_back_amount')->nullable();
            $table->text('per_returned_ach_item_amount')->nullable();
            $table->tinyInteger('limit_charge_back')->nullable();
            $table->text('limit_charge_back_amount')->nullable();
            $table->tinyInteger('reserve_account')->nullable();
            $table->text('reserve_account_amount')->nullable();
            $table->text('gross_transaction_volume')->nullable();
            $table->tinyInteger('bounding_amount')->nullable();
            $table->text('bounding_amount_amount')->nullable();
            // bank info
            $table->text('bank_applicant_legal_name')->nullable();
            $table->text('bank_applicant_title')->nullable();
            $table->text('bank_applicant_email')->nullable();
            $table->text('bank_applicant_phone_no')->nullable();
            $table->text('bank_business_name')->nullable();
            $table->text('bank_business_ein')->nullable();
            $table->text('bank_business_registered_ficitious_name')->nullable();
            $table->text('bank_business_corporate_address')->nullable();
            $table->text('bank_business_phone_no')->nullable();
            $table->text('bank_business_preferred_email')->nullable();
            $table->dateTime('bank_business_date_established')->nullable();
            $table->dateTime('bank_business_current_ownership')->nullable();
            $table->tinyInteger('bank_business_type')->nullable()
                ->comment('1 solo proprietor 2: corporation 3:llc 4: partnership 5: llp 6: trust 7: other');
            $table->text('bank_business_type_other')->nullable();
            $table->text('bank_business_products')->nullable();
            $table->text('bank_business_operations')->nullable();
            $table->text('bank_business_current_licenses')->nullable();
            $table->text('bank_business_monthly_sales')->nullable();

            $table->boolean('questionnaire_form_status')->default(0)->comment('1: complete 0: not complete');
            $table->boolean('application_status')->default(1)->comment('1: auto save 2: save by user');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
