<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShareApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('share_applications', function (Blueprint $table) {
            $table->string('id', 25)->primary();
            $table->string('application_id', 25)->index();
            $table->string('user_id', 25)->index();
            $table->tinyInteger('status')->default(1)->comment('1: active share 0 close sharing');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('share_applications');
    }
}
