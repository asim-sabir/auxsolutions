<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'Auth\LoginController@login')->name('admin.login');
Route::post('/loginPost', 'Auth\LoginController@loginPost')->name('admin.login.post');
Route::get('/logout', 'Auth\LoginController@logout')->name('admin.logout');

Route::group(['middleware' => 'auth.admin'], function () {
    Route::get('/', 'HomeController@home')->name('admin.home');

    Route::group(['prefix' => 'application'], function () {
        Route::get('/list', 'ApplicationController@index')->name('admin.application.list');
        Route::post('store/questionnaire-form', 'ApplicationController@storeQuestionnaireForm')->name('admin.application.store.questionnaire.form');
        Route::post('/store', 'ApplicationController@store')->name('admin.application.store');
        Route::get('/edit/{application}', 'ApplicationController@edit')->name('admin.application.edit');
        Route::get('/delete/{application}', 'ApplicationController@destroy')->name('admin.application.destroy');
    });
    Route::group(['prefix' => 'agent'], function () {
        Route::get('/list', 'AgentController@index')->name('admin.agent.list');
        Route::get('/delete/{user}', 'AgentController@destroy')->name('admin.agent.destroy');
    });
    Route::group(['prefix' => 'customer'], function () {
        Route::get('/list', 'CustomerController@index')->name('admin.customer.list');
        Route::get('/delete/{user}', 'CustomerController@destroy')->name('admin.customer.destroy');
    });
});