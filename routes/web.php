<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'panel', 'namespace' => 'Panel', 'middleware' => 'auth'], function () {
    Route::get('/', [
        'uses' => 'HomeController@index',
        'as' => 'panel.home'
    ]);
    // applications
    Route::group(['prefix' => 'applications'], function () {
        Route::get('/', [
            'uses' => 'ApplicationController@index',
            'as' => 'panel.application'
        ]);
        Route::get('new/{application_no}', [
            'uses' => 'ApplicationController@create',
            'as' => 'panel.application.new'
        ]);
        Route::post('auto-store/questionnaire-form', 'ApplicationController@autoStoreQuestionnaireForm')->name('panel.application.auto.store.questionnaire.form');
        Route::post('store/questionnaire-form', 'ApplicationController@storeQuestionnaireForm')->name('panel.application.store.questionnaire.form');
        Route::post('store', 'ApplicationController@store')->name('panel.application.store');
        Route::get('edit/{application_no}', 'ApplicationController@create')->name('panel.application.edit');
        Route::get('customer/{application_no}', 'ApplicationController@agentApplication')->name('panel.application.edit.customer');
        Route::group(['prefix' => 'share'], function () {
            Route::post('/', 'ApplicationController@share')->name('panel.application.share');
            Route::post('/merchant', 'ApplicationController@shareWithMerchant')->name('panel.application.share.merchant');
        });
    });
});
Route::group(['prefix' => 'customer', 'namespace' => 'Customer'], function () {
    Route::group(['prefix' => 'attachment'], function () {
        Route::get('/{token}', [
            'uses' => 'AttachmentController@agreement',
            'as' => 'customer.partner.agreement'
        ]);
        Route::post('/upload', [
            'uses' => 'AttachmentController@upload',
            'as' => 'customer.partner.upload'
        ]);
    });
});
