function sideBarToggle(class_name = 'sidebar-xs') {
    let body_obj = $('body');
    if (body_obj.hasClass(class_name)) {
        body_obj.removeClass(class_name)
        return true;
    }
    body_obj.addClass(class_name)
    return true;
}

function dateRangeSingle() {
    $('.daterange-single').daterangepicker({
        singleDatePicker: true
    });
}

/**
 * bg-primary
 * bg-danger
 * bg-success
 * bg-warning
 * bg-info
 * */
function pNotify(title, text, add_class) {
    new PNotify({
        title: title,
        text: text,
        addclass: 'bg-' + add_class + ' border-' + add_class
    });
}

function setLoading(enable) {
    const loader = $('#loader_data');
    if (enable) {
        $(loader).removeClass('hide_elements');
    } else {
        $(loader).addClass('hide_elements');
    }
}

function componentUniform() {
    if (!$().uniform) {
        console.warn('Warning - uniform.min.js is not loaded.');
        return;
    }

    // Initialize
    $('.form-input-styled').uniform({
        fileButtonClass: 'action btn bg-blue'
    });
}

function componentSelect2() {
    if (!$().select2) {
        console.warn('Warning - select2.min.js is not loaded.');
        return;
    }

    // Initialize
    var $select = $('.form-control-select2').select2({
        minimumResultsForSearch: Infinity,
        width: '100%'
    });

    // Trigger value change when selection is made
    $select.on('change', function () {
        $(this).trigger('blur');
    });
}

// Switchery
function componentSwitchery() {
    if (typeof Switchery == 'undefined') {
        console.warn('Warning - switchery.min.js is not loaded.');
        return;
    }
    // Initialize multiple switches
    var elems = Array.prototype.slice.call(document.querySelectorAll('.form-input-switchery'));
    elems.forEach(function (html) {
        var switchery = new Switchery(html);
    });
}

// Bootstrap switch
function componentBootstrapSwitch() {
    if (!$().bootstrapSwitch) {
        console.warn('Warning - switch.min.js is not loaded.');
        return;
    }
    // Initialize
    $('.form-check-input-switch').bootstrapSwitch();
};