function httpPost(url, data, callback) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.post(url, data)
        .done(function (response) {
            callback(response, true);
        })
        .fail(function (response) {
            callback(response, true);
        })
}