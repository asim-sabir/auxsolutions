let api = {
    'send-to-customer': 'applications/share'
};

function getPanelApi(key) {
    return '/panel/' + api[key];
}
