$(function () {
    $('.form-check-input-switch').on('switchChange.bootstrapSwitch', function (event, state) {

    });
    $('.fee-schedule-form-payment').click(function () {
        const self = $(this);
        const title = self.data('title');
        $('#model-title').html(title);
        $('#fee-model-input').val(self.html());
        $('#fee-model-input-id-name').val(self.data('inputid'));
        $('#fee-model-trigger-class-name').val(self.data('class'));
    })
    $('#fee-model-input').keyup(function () {
        const val = $(this).val();
        const id_name = $('#fee-model-input-id-name').val();
        const class_name = $('#fee-model-trigger-class-name').val();
        $('#' + id_name).val(val)
        $('.' + class_name).html(val)
    })

    $('#share-application').click(function () {
        $('#application-id').val($(this).data('id'));
    })

    $('#send-application').click(function () {
        setLoading(true);
        httpPost(form_url, $(this).parents('form').serialize(), function (response, response_type) {
            if (response_type) {
                if (response.isResponse) {
                    pNotify('', response.message, 'success');
                } else {
                    pNotify('', response.message, 'danger');
                }
            }
            setLoading(false);
        });
    })

    $('#questionnaire-form').submit(function (e) {
        e.preventDefault();
        const fee_schedule_form_modal_obj = $('#fee_schedule_form_modal');
        if (fee_schedule_form_modal_obj.attr('data-opened') > 0) {
            this.submit();
        } else {
            fee_schedule_form_modal_obj.attr('data-opened', 1);
            fee_schedule_form_modal_obj.modal('show');
        }
    })
});

function applicationPostInfo(form_url, data) {
    setLoading(true);
    httpPost(form_url, data, function (response, response_type) {
        if (response_type) {
            if (response.isResponse) {
                pNotify('', response.message, 'success');
            } else {
                pNotify('', response.message, 'danger');
            }
        }
        setLoading(false);
    });
}

function applicationAutoPostInfo(form_url, data) {
    httpPost(form_url, data, function (response, response_type) {
        if (response_type) {
        }
    });
}

function creditCardSetup() {
    if (!$().steps) {
        console.warn('Warning - steps.min.js is not loaded.');
        return;
    }
    // Basic wizard setup
    $('.credit-card-steps-basic').steps({
        headerTag: 'h6',
        bodyTag: 'fieldset',
        transitionEffect: 'fade',
        titleTemplate: '<span class="number">#index#</span> #title#',
        labels: {
            previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
            next: 'Next <i class="icon-arrow-right14 ml-2" />',
            finish: 'Save <i class="icon-arrow-right14 ml-2 finishedAndSaved" />'
        },
        onFinished: function (event, currentIndex) {
            let form_obj = $('#credit_card_form');
            let form_url = form_obj.attr('action');
            applicationPostInfo(form_url, form_obj.serialize());
        }
    });
    if (!$().validate) {
        console.warn('Warning - validate.min.js is not loaded.');
        return;
    }

}

function bankingSetup() {
    if (!$().steps) {
        console.warn('Warning - steps.min.js is not loaded.');
        return;
    }
    // Basic wizard setup
    $('.banking-steps-basic').steps({
        headerTag: 'h6',
        bodyTag: 'fieldset',
        transitionEffect: 'fade',
        titleTemplate: '<span class="number">#index#</span> #title#',
        labels: {
            previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
            next: 'Next <i class="icon-arrow-right14 ml-2" />',
            finish: 'Save <i class="icon-arrow-right14 ml-2 finishedAndSaved" />'
        },
        onFinished: function (event, currentIndex) {
            let form_obj = $('#banking_form');
            let form_url = form_obj.attr('action');
            let form_data = form_obj.serialize();
            form_data += '&application_status=2';
            applicationPostInfo(form_url, form_data);
        }
    });
    if (!$().validate) {
        console.warn('Warning - validate.min.js is not loaded.');
        return;
    }

}

document.addEventListener('DOMContentLoaded', function () {
    creditCardSetup();
    bankingSetup();
    componentUniform();
    componentSelect2();
    componentSwitchery();
    componentBootstrapSwitch();
});
