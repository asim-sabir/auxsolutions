var LoginRegistration = function () {
    // Uniform
    var _componentUniform = function () {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }
        // Initialize
        $('.form-input-styled').uniform();
    };
    return {
        initComponents: function () {
            _componentUniform();
        }
    }
}();
document.addEventListener('DOMContentLoaded', function () {
    LoginRegistration.initComponents();
});
