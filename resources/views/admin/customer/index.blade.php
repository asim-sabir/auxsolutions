@extends('layouts.admin.default')
@section('title', 'Customer')
@section('contents')
    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">Customer</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('layouts.messages')
                            </div>
                        </div>

                        <table class="table text-center datatable-basic table-bordered table-striped table-hover">
                            <thead>
                            <tr class="text-center">
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Join Date</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!count($customers))
                                <tr>
                                    <td colspan="6" class="py-4 text-center">No record found..</td>
                                </tr>
                            @endif
                            @foreach($customers as $k => $v)
                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{ $v->first_name .' ' . $v->last_name }}</td>
                                    <td>{{ $v->email }}</td>
                                    <td>{{ $v->created_at->format('Y-m-d') }}</td>
                                    <td>
                                        <span class="badge badge-{{ $v->status == 1 ? 'success' : 'secondary' }}">
                                            {{ $v->status == 1? 'Active' : 'Inactive' }}
                                        </span>
                                    </td>
                                    <td>
                                        @if($v->status == 1)
                                            <a href="#" class="mx-2">
                                                <i class="icon-cross text-primary"></i>
                                            </a>
                                        @else
                                            <a href="#" class="mx-2">
                                                <i class="icon-check text-primary"></i>
                                            </a>
                                        @endif
                                        <a href="#" class="mx-2">
                                            <i class="icon-pencil text-primary"></i>
                                        </a>
                                        <a href="{{ route('admin.customer.destroy', $v->id) }}" class="mx-2 delete">
                                            <i class="icon-trash text-primary"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @parent
    <script>
        $('.delete').click(function () {
            alert('Do you really want to delete this record')
        });
    </script>
@endsection