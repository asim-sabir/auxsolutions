@extends('layouts.admin.default')
@section('title', 'Application')
@section('contents')
    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">Application</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('layouts.messages')
                            </div>
                        </div>
                        <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 float-right">
                            <li class="nav-item"><a href="#pending" class="nav-link active" data-toggle="tab">Pending</a></li>
                            <li class="nav-item"><a href="#complete" class="nav-link" data-toggle="tab">Complete</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="pending">
                                <table class="table text-center datatable-basic table-bordered table-striped table-hover">
                                    <thead>
                                    <tr class="text-center">
                                        <th scope="col">#</th>
                                        <th scope="col">Agent Name</th>
                                        <th scope="col">Business Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Phone No</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">View Licence</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!count($pending))
                                        <tr>
                                            <td colspan="8" class="py-4 text-center">No record found..</td>
                                        </tr>
                                    @endif
                                    @foreach($pending as $k => $v)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{ $v->agentInfo->first_name .' ' . $v->agentInfo->last_name }}</td>
                                            <td>{{ $v->mmj_merchant_business_name }}</td>
                                            <td>{{ $v->mmj_merchant_email }}</td>
                                            <td>{{ $v->mmj_merchant_business_phone_no }}</td>
                                            <td>{{ $v->created_at->format('Y-m-d') }}</td>
                                            <td>
                                                @if($v->merchant_licence)
                                                    <a href="{{ asset('uploads/confidential/'. $v->user_id .'/'. $v->merchant_licence) }}" target="_blank">
                                                        view
                                                    </a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <div class="list-icons">
                                                    <div class="dropdown">
                                                        <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                            <i class="icon-menu9"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a href="{{ route('admin.application.edit', $v->id) }}" class="dropdown-item"><i class="icon-pencil mr-2"></i> Edit</a>
                                                            <a href="{{ route('admin.application.destroy', $v->id) }}" class="dropdown-item delete"><i class="icon-trash mr-2"></i> Delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="complete">
                                <table class="table text-center datatable-basic table-bordered table-striped table-hover">
                                    <thead>
                                    <tr class="text-center">
                                        <th scope="col">#</th>
                                        <th scope="col">Agent Name</th>
                                        <th scope="col">Business Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Phone No</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">View Licence</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!count($complete))
                                        <tr>
                                            <td colspan="8" class="py-4 text-center">No record found..</td>
                                        </tr>
                                    @endif
                                    @foreach($complete as $k => $v)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{ $v->agentInfo->first_name .' ' . $v->agentInfo->last_name }}</td>
                                            <td>{{ $v->mmj_merchant_business_name }}</td>
                                            <td>{{ $v->mmj_merchant_email }}</td>
                                            <td>{{ $v->mmj_merchant_business_phone_no }}</td>
                                            <td>{{ $v->created_at->format('Y-m-d') }}</td>
                                            <td>
                                                @if($v->merchant_licence)
                                                    <a href="{{ asset('uploads/confidential/'. $v->user_id .'/'. $v->merchant_licence) }}" target="_blank">
                                                        view
                                                    </a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <div class="list-icons">
                                                    <div class="dropdown">
                                                        <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                            <i class="icon-menu9"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a href="{{ route('admin.application.edit', $v->id) }}" class="dropdown-item"><i class="icon-pencil mr-2"></i> Edit</a>
                                                            <a href="{{ route('admin.application.destroy', $v->id) }}" class="dropdown-item delete"><i class="icon-trash mr-2"></i> Delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $('.delete').click(function () {
            alert('Do you really want to delete this record')
        });
    </script>
@endsection
