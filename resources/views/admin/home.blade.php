@extends('layouts.admin.default')
@section('title','home')
@section('contents')
    <div class="content">
        <div class="page-content">
            <div class="content-wrapper">
                <div class="content pt-4">
                    <div class="row">
                        <div class="col-sm-6 col-xl-6">
                            <div class="card card-body bg-blue-400 has-bg-image">
                                <div class="media">
                                    <div class="media-body"><h3 class="mb-0">{{ $customers }}</h3><span class="text-uppercase font-si ze-xs">total Customer</span>
                                    </div>
                                    <div class="ml-3 align-self-center"><i class="icon-users4 icon-3x opacity-75"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-6">
                            <div class="card card-body bg-danger-400 has-bg-image">
                                <div class="media">
                                    <div class="media-body"><h3 class="mb-0">{{ $merchants }}</h3><span class="text-uppercase font-size-xs">Total Merchant</span>
                                    </div>
                                    <div class="ml-3 align-self-center"><i class="icon-users4 icon-3x opacity-75"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
