@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error') }}
                    </div>
                @endif
                <div class="card-header">{{ __('Attachment') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('customer.partner.upload') }}" enctype="multipart/form-data">
                        <input type="hidden" name="link_token" value="{{ $user->link_to_file_upload }}">
                        @csrf
                        <!-- first name -->
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="file" class="form-control{{ $errors->has('fileToUpload') ? ' is-invalid' : '' }}" name="fileToUpload" required autofocus>
                                @if ($errors->has('fileToUpload'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('fileToUpload') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Upload') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
