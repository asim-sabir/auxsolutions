@extends('layouts.panel.default', ['page' => 'Application# ' . $application_no])
@section('css')
    @parent
    <style>
        .wizard>.content{
            padding: 20px !important;
        }
        .form-control{
            height: 3.25003rem !important;
            border: 2px solid #ddd !important;
            border-radius: 0px !important;
        }
        .fee-schedule-form-payment{
            font-weight: bold;
            text-decoration: underline !important;
            font-size: 15px;
        }
    </style>
@endsection
@section('contents')
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="card">
                <div class="card-body">
                    @include('layouts.general.messages')
                    <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 float-right">
                        <li class="nav-item"><a href="#questionnaire" class="nav-link active" data-toggle="tab">Questionnaire</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="questionnaire">
                            <form id="questionnaire-form" method="post" action="{{ route('panel.application.store.questionnaire.form') }}" enctype="multipart/form-data">
                                @csrf
                                @include('application.questionnaire.index')
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit Application</button>
                                    <button type="button" class="btn btn-dark" id="send-to-customer">Send Link <i class="icon-link ml-2"></i></button>
                                </div>
                                <!-- fee schedule form-->
                                <div id="fee_schedule_form_modal" class="modal fade" tabindex="-1" data-opened="0">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="model-title">Fee Schedule</h5>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body" style=" padding: 2.25rem !important;">
                                                @include('application.credit_card.fee_schedule')
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary">Submit Application</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- fee schedule form-->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- set value -->
    <div id="fee_schedule_form_values" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="model-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style=" padding: 2.25rem !important;">
                    <div class="input-group">
                        <input type="text" class="form-control" id="fee-model-input">
                        <input type="hidden" class="form-control" id="fee-model-input-id-name">
                        <input type="hidden" class="form-control" id="fee-model-trigger-class-name">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- set value -->
@endsection
@section('scripts')
    @parent
    <script src="{{asset('js/plugins/pickers/daterangepicker.js')}}"></script>
    <script>
        $(function () {
            sideBarToggle();
            dateRangeSingle();
            $('#send-to-customer').click(function () {
                let url = '{{ route('panel.application.share') }}';
                let data = {
                    application_id: $('#application_no').val(),
                    email: $('#questionnaire_email').val()
                };
                applicationPostInfo(url, data);
            })
        })
    </script>
    <script src="{{asset('js/pages/application.js')}}"></script>
@endsection
