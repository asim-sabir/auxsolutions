@extends('layouts.panel.default' , ['page' => 'Dashboard'])
@section('contents')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
@endsection
