@extends('layouts.general.mail')
@section('contents')
    <div style="text-align: center;font-size: 26px;color: #57b029;font-weight: bold;padding-bottom: 50px;">
        Welcome
    </div>
    Hello {{ $data->first_name }},
    <p>Welcome to your new Back office portal by AuxPAY Solution. Please click the link below to log into to start your application processing workflow.</p>
    <br>
    <p>
        <a href="{{ $link }}" target="_blank" style="text-decoration: none; background-color: #57b029 !important;color: #fff;border: 2px solid #57b029 !important;min-width: 150px;padding: 9px 5px;border-radius: 5px;text-transform: uppercase;font-size: 16px;margin-right: 10px;margin-bottom: 15px;letter-spacing: 2px;transition: all 0.4s ease-in-out;-webkit-transition: all 0.4s ease-in-out;">
            <span style="color: #ffffff;">
                AuxPAY
            </span>
        </a>
    </p>
    <br/>
    Thanks,
    <br/>
    AuxPAY Team
@endsection


