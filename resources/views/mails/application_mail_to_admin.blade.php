<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body {
            background-color: #fff;
        }
        .frame.en{
            padding: 50px 200px;
            background-color: #f7f7f7;
            margin: 5px;
            font-family: 'Montserrat', sans-serif !important;
        }

        .frame.cn{
            padding: 50px 200px;
            background-color: #f7f7f7;
            margin: 5px;
            font-family: "Microsoft YaHei" !important;
        }

        .content{
            background-color: #fff;
            border-radius: 15px;
            padding: 60px;
        }

        .title{
            text-align: center;
            font-size: 26px;
            color: #57b029;
            font-weight: bold;
            padding-bottom: 100px;
        }

        .img-sec{
            padding: 20px 0px;
            text-align: center;
            margin-bottom: 50px;
        }

        .footer{
            text-align: center;
            padding: 10px;
            color: #a4a4a4;
            font-size: 14px;
            font-weight: bold;
        }

        .footer hr{
            width: 50%;
            color: #dcdfe0;
            margin-bottom: 10px;
            border-left: 0px;
            border-top: 0px;
            border-bottom: solid 1px;
        }

        a{
            text-decoration: none;
        }
        @media only screen and (max-width:320px) { @viewport { width:320px; } }
    </style>
</head>
<body style="background-color: #fff;">
<div class="frame en" style="padding: 50px 200px;  background-color: #f7f7f7;  margin: 5px;  font-family: 'Montserrat', sans-serif !important;">

    <div class="content" style="background-color: #fff;  border-radius: 15px;  padding: 60px;">
        <div class="title" style="text-align: center;  font-size: 26px;  color: #57b029;  font-weight: bold;  padding-bottom: 50px;">
            New Application
        </div>
        Hello,
        <p>{{ $merchant_name }} is submitted the new application# {{ $data->id }}</p>
        <p>
            <a href="{{ url($link) }}" target="_blank" style="text-decoration: none; background-color: #57b029 !important;color: #fff;border: 2px solid #57b029 !important;min-width: 150px;padding: 9px 5px;border-radius: 5px;text-transform: uppercase;font-size: 16px;margin-right: 10px;margin-bottom: 15px;letter-spacing: 2px;transition: all 0.4s ease-in-out;-webkit-transition: all 0.4s ease-in-out;">
                <span class="link" style="color: #ffffff;">
                    Application
                </span>
            </a>
            <br><br>
            <span class="link" style="color: #57b029;">{{ url($link) }}</span>
        </p>
        <br/>
        Thanks,
        <br/>
        AUX team
    </div>
    <div class="footer" style="text-align: center;  padding: 10px;  color: #a4a4a4;  font-size: 14px;  font-weight: bold;">
        <br/>
    </div>
</div>
</body>
</html>


