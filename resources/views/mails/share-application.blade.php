@extends('layouts.general.mail')
@section('contents')
    <div style="text-align: center;font-size: 26px;color: #57b029;font-weight: bold;padding-bottom: 50px;">
        Application# {{ $data->application_id }}
    </div>
    Hello,
    <p>Thank you for your interest in moving forward with the true solution of Cannabis . Please login to your account and proceed to application# <b>{{ $data->application_id }}</b>
    </p>
    <br>
    <p><b>Email: </b> {{ $data->email }}</p>
    @if($data->new_user)
        <p><b>Password: </b> {{ $data->application_id }}</p>
    @endif
    <p>
        <a href="{{ url('/login') }}" target="_blank" style="text-decoration: none; background-color: #0277BD !important;color: #fff;border: 2px solid #0277BD !important;min-width: 150px;padding: 9px 5px;border-radius: 5px;text-transform: uppercase;font-size: 14px;margin-right: 10px;margin-bottom: 15px;letter-spacing: 2px;transition: all 0.4s ease-in-out;-webkit-transition: all 0.4s ease-in-out;">
            <span style="color: #ffffff;">
                View Your Application
            </span>
        </a>
    </p>
    <br/>
    Thanks,
    <br/>
    AuxPAY Team
@endsection



