<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        @media only screen and (max-width: 320px) {
            @viewport {
                width: 320px;
            }
        }
    </style>
</head>
<body  style="background-color: #fff;">
    <div style="padding: 50px 200px;background-color: #f7f7f7;margin: 5px;font-family: 'Montserrat', sans-serif !important;">
        <div class="content" style="background-color: #fff;border-radius: 15px;padding: 60px;">
            @yield('contents')
        </div>
    </div>
    @section('footer')
        <div class="footer" style="text-align: center;padding: 10px;color: #a4a4a4;font-size: 14px;font-weight: bold;">
            <br/>
        </div>
    @show
</body>
</html>
