<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{{asset('css/general/icomoon.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/panel/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/panel/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/panel/layout.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/panel/components.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/panel/colors.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/general/custom.css')}}" rel="stylesheet" type="text/css">

