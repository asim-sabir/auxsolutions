<!-- js files -->
<script src="{{asset('js/general/jquery.min.js')}}"></script>
<script src="{{asset('js/general/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('js/panel/app.js')}}"></script>
<!-- others -->
<script src="{{asset('js/others/api.js')}}"></script>
<script src="{{asset('js/others/custom.js')}}"></script>
<script src="{{asset('js/others/http.requests.js')}}"></script>
<!-- plugins -->
<script src="{{asset('js/plugins/loaders/blockui.min.js')}}"></script>
<script src="{{asset('js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('js/plugins/forms/styling/switchery.min.js')}}"></script>
<script src="{{asset('js/plugins/forms/styling/switch.min.js')}}"></script>
<script src="{{asset('js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('js/plugins/ui/moment/moment.min.js')}}"></script>
<script src="{{asset('js/plugins/notifications/pnotify.min.js')}}"></script>
<!-- js files -->

