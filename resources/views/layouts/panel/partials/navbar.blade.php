<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark">
    <!-- Header with logos -->
    <div class="navbar-header navbar-dark d-none d-md-flex align-items-md-center">
        <div class="navbar-brand pt-0 pb-0">
            <a href="#" class="d-inline-block">
                <img src="{{asset('img/logo/logo.png')}}" alt="" style="height: 2rem;margin: 20px 0;">
            </a>
        </div>

        <div class="navbar-brand navbar-brand-xs">
            <a href="#" class="d-inline-block">
                <img src="{{asset('img/logo/logo.png')}}" alt="">
            </a>
        </div>
    </div>
    <!-- Mobile controls -->
    <div class="d-flex flex-1 d-md-none">
        <div class="navbar-brand mr-auto">
            <a href="#" class="d-inline-block">
                <img src="{{asset('img/logo/logo.png')}}" alt="">
            </a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>
    <!-- /mobile controls -->
    <div class="collapse navbar-collapse" id="navbar-mobile">
        <span class="badge bg-success ml-md-3 mr-md-auto" style="opacity: 0; height: 0px;">&nbsp;</span>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>
        @if(auth()->check())
            <ul class="navbar-nav">
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                        <span class="text-capitalize">
                            <span class="badge bg-success-400 mr-2" style="position: static;">Active</span>
                            {{auth()->user()->first_name .' '. auth()->user()->last_name}}
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="icon-switch2"></i> {{ __('Logout') }}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        @endif
    </div>
</div>
<!-- /main navbar -->
