<!-- Main sidebar -->
<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">
    <div class="card-body p-0">
        <ul class="nav nav-sidebar" data-nav-type="accordion">
            <!-- Main -->
            <li class="nav-item">
                <a href="{{ route('panel.home') }}"
                   class="nav-link {{ Request::url() == route('panel.home') ? 'active' : '' }}">
                    <i class="icon-home4"></i>
                    <span>
                        Home
                    </span>
                </a>
            </li>
            <!-- application -->
            <li class="nav-item">
                <a href="{{ route('panel.application') }}"
                   class="nav-link {{ Request::url() == route('panel.application') ? 'active' : '' }}">
                    <i class="icon-copy"></i>
                    <span>
                        Application
                    </span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- /main sidebar -->
