<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name') }} - @yield('title')</title>
    @section('css')
        @include('layouts.panel.partials.css')
    @show
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    @include('layouts.panel.partials.navbar')
    @section('page-header')
        @include('layouts.panel.partials.pageHeader')
    @show
    <!-- Page content -->
    <div class="page-content pt-0">
    @include('layouts.panel.partials.sideBar')
        <!-- Main content -->
        <div class="content-wrapper">
            <div class="content">
                @yield('contents')
            </div>
        </div>
    </div>
    @include('layouts.panel.partials.footer')
    <div class="loader_data hide_elements" id="loader_data">
        <div class="loader_data_center">
            <div class="pace-demo text-center bg-teal-800">
                <div class="theme_radar theme_radar_with_text">
                    <div class="pace_progress" data-progress-text="60%" data-progress="60"></div>
                    <div class="pace_activity"></div>
                    <span>Loading Data</span>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
@section('scripts')
    @include('layouts.panel.partials.scripts')
@show
