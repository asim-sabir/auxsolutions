<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title') - {{ config('app.name') }}</title>
    @section('css')
        @include('layouts.admin.partials.css')
    @show
</head>
<body>
@include('layouts.admin.partials.navbar')
<!-- Page content -->
<div class="page-content">
@include('layouts.admin.partials.sidebar')
<!-- Main content -->
    <div class="content-wrapper">
        @yield('contents')
    </div>
</div>
@include('layouts.admin.partials.footer')
</body>
</html>
@section('scripts')
    @include('layouts.admin.partials.scripts')
@show