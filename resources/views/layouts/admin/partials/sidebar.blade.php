<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md" style=" margin:0px;">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->
    <!-- Sidebar content -->
    <div class="sidebar-content">
        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">

                <div class="media">
                    <div class="mr-3">
                        <a href="/">
                            <img src="{{ asset('img/profile/default/default.jpg') }}" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>
                    <div class="media-body">
                        <div class="media-title font-weight-semibold">{{Auth::guard('admin')->user()->name}}</div>
                        <div class="font-size-xs opacity-50">
                            <i class="icon-pin font-size-sm"></i> &nbsp;
                        </div>
                    </div>
                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>

            </div>
        </div>
        <!-- /User menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Main</div>
                    <i class="icon-menu" title="Main"></i>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.home')}}" class="nav-link {{ Request::url() == route('admin.home') ? 'active' : '' }}">
                        <i class="icon-home4"></i>
                        <span>
                            Dashboard
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.application.list')}}" class="nav-link {{ Request::url() == route('admin.application.list') ? 'active' : '' }}">
                        <i class="icon-copy"></i>
                        <span>
                            Application
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.agent.list')}}" class="nav-link {{ Request::url() == route('admin.agent.list') ? 'active' : '' }}">
                        <i class="icon-users4"></i>
                        <span>
                            Agent
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.customer.list')}}" class="nav-link {{ Request::url() == route('admin.customer.list') ? 'active' : '' }}">
                        <i class="icon-users4"></i>
                        <span>
                            Customer
                        </span>
                    </a>
                </li>
                <!-- /main -->
            </ul>
        </div>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar content -->
</div>
<!-- /main sidebar -->