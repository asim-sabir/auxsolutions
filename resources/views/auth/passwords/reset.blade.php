@extends('auth.default')
@section('content')
    <!-- Password recovery form -->
    <form class="login-form" method="POST" action="{{ route('password.update') }}">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="card mb-0">
            <div class="card-body">
                <div class="text-center mb-3">
                    <i class="icon-spinner11 icon-2x text-warning border-warning border-3 rounded-round p-3 mb-3 mt-1"></i>
                    <h5 class="mb-0">{{ __('Reset Password') }}</h5>
                </div>
                <div class="form-group">
                    @include('layouts.general.messages')
                </div>
                <!-- email -->
                <div class="form-group form-group-feedback form-group-feedback-right">
                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Your email" value="{{ old('email') }}" name="email">
                    <div class="form-control-feedback">
                        <i class="icon-mail5 text-muted"></i>
                    </div>
                </div>
                <!-- password -->
                <div class="form-group form-group-feedback form-group-feedback-right">
                    <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required />
                    <div class="form-control-feedback">
                        <i class="icon-lock2 text-muted"></i>
                    </div>
                </div>
                <!-- confirm password -->
                <div class="form-group form-group-feedback form-group-feedback-right">
                    <input type="password" class="form-control" name="password_confirmation" required placeholder="Confirmation password"/>
                    <div class="form-control-feedback">
                        <i class="icon-lock2 text-muted"></i>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn bg-blue btn-block"><i class="icon-spinner11 mr-2"></i> {{ __('Reset Password') }}</button>
                </div>
            </div>
        </div>
    </form>
    <!-- /password recovery form -->
@endsection
