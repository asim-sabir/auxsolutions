@extends('auth.default')
@section('content')
    <!-- Password recovery form -->
    <form class="login-form" method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="card mb-0">
            <div class="card-body">
                <div class="text-center mb-3">
                    <i class="icon-spinner11 icon-2x text-warning border-warning border-3 rounded-round p-3 mb-3 mt-1"></i>
                    <h5 class="mb-0">{{ __('Password recovery') }}</h5>
                    <span class="d-block text-muted">We'll send you instructions in email</span>
                </div>
                <div class="form-group">
                    @include('layouts.general.messages')
                </div>
                <div class="form-group form-group-feedback form-group-feedback-right">
                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Your email" value="{{ old('email') }}" name="email" required>
                    <div class="form-control-feedback">
                        <i class="icon-mail5 text-muted"></i>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn bg-blue btn-block"><i class="icon-spinner11 mr-2"></i> {{ __('Send Password Reset Link') }}</button>
                </div>
                <div class="form-group text-center text-muted content-divider">
                    <span class="px-2">Don't have an account?</span>
                </div>
                <div class="form-group">
                    <a href="{{ route('register') }}" class="btn btn-light btn-block">Sign up</a>
                </div>
            </div>
        </div>
    </form>
    <!-- /password recovery form -->
@endsection