@extends('auth.default')
@section('content')
    <!-- registration card -->
    <form class="flex-fill" method="POST" action="{{ route('register') }}">
        @csrf
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="card mb-0">
                    <div class="card-body">
                        <div class="text-center mb-3">
                            <i class="icon-plus3 icon-2x text-success border-success border-3 rounded-round p-3 mb-3 mt-1"></i>
                            <h5 class="mb-0">{{ __('Create account') }}</h5>
                            <span class="d-block text-muted">{{ __('All * fields are required') }}</span>
                        </div>
                        <div class="form-group">
                            @include('layouts.general.messages')
                        </div>
                        <!-- email -->
                        <div class="form-group form-group-feedback form-group-feedback-right">
                            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email *" name="email" value="{{ old('email') }}" required autofocus>
                            <div class="form-control-feedback">
                                <i class="icon-mention text-muted"></i>
                            </div>
                        </div>
                        <!-- names row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="First name *" name="first_name" value="{{ old('first_name') }}" required autofocus>
                                    <div class="form-control-feedback">
                                        <i class="icon-user-check text-muted"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="text" class="form-control" placeholder="Second name" name="last_name" value="{{ old('last_name') }}">
                                    <div class="form-control-feedback">
                                        <i class="icon-user-check text-muted"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- password row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Create password *" required>
                                    <div class="form-control-feedback">
                                        <i class="icon-user-lock text-muted"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="password" class="form-control" placeholder="Confirm password *" name="password_confirmation" required>
                                    <div class="form-control-feedback">
                                        <i class="icon-user-lock text-muted"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- phone no -->
                        <div class="form-group form-group-feedback form-group-feedback-right">
                            <input type="text" class="form-control{{ $errors->has('phone_no') ? ' is-invalid' : '' }}" placeholder="Phone no *" name="phone_no" value="{{ old('phone_no') }}" required autofocus>
                            <div class="form-control-feedback">
                                <i class="icon-phone text-muted"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-plus3"></i></b>
                                {{ __('Create account') }}
                            </button>
                        </div>
                        <div class="form-group text-center text-muted content-divider">
                            <span class="px-2">Do you have an account?</span>
                        </div>
                        <div class="form-group">
                            <a href="{{ route('login') }}" class="btn btn-light btn-block">Sign in</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- /registration card -->
@endsection
