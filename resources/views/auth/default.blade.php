<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name') }} - @yield('title')</title>
    @section('css')
        @include('layouts.panel.partials.css')
    @show
</head>
<body class="bg-slate-800">
    <!-- Page content -->
    <div class="page-content">
        <!-- Main content -->
        <div class="content-wrapper">
            <div class="content d-flex justify-content-center align-items-center">
                @yield('content')
            </div>
        </div>
    </div>
    @include('layouts.panel.partials.footer')
</body>
</html>
@section('scripts')
    @include('layouts.panel.partials.scripts')
@show
