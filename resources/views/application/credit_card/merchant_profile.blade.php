<fieldset>
    <div class="row">
        <!-- type of entity -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Type of Entity:</label>
                <input type="text" name="type_of_entity" class="form-control" value="{{ $application->type_of_entity }}" placeholder="type of entity">
            </div>
        </div>
        <!-- type -->
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-lg-3 col-form-label">Type:</label>
                <div class="col-lg-12">
                    <div class="form-check form-check-inline col-md-6">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="firm_type" data-fouc value="1" {{ $application->firm_type == 1 ? 'checked' : '' }}>
                            Sole Proprietor
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="firm_type" data-fouc value="2" {{ $application->firm_type == 2 ? 'checked' : '' }}>
                            Partnership
                        </label>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-check form-check-inline col-md-6">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="firm_type" data-fouc value="3" {{ $application->firm_type == 3 ? 'checked' : '' }}>
                            LLC
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="firm_type" data-fouc value="4" {{ $application->firm_type == 4 ? 'checked' : '' }}>
                            Corporation
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Type of Goods Sold -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Type of Goods Sold:</label>
                <input type="text" name="type_of_goods_sold" class="form-control" value="{{ $application->type_of_goods_sold }}" placeholder="type of goods sold">
            </div>
        </div>
        <!-- type -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Web Address:</label>
                <input type="text" name="web_address" class="form-control" value="{{ $application->web_address }}" placeholder="web address">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Length of Ownership  -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Length of Ownership:</label>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="length_of_ownership_yrs" class="form-control" value="{{ $application->length_of_ownership_yrs }}" placeholder="yrs">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="length_of_ownership_mos" class="form-control" value="{{ $application->length_of_ownership_mos }}" placeholder="mos">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Length of Time at Location -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Length of Time at Location:</label>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="length_of_time_at_location_yrs" class="form-control" value="{{ $application->length_of_time_at_location_yrs }}" placeholder="yrs">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="length_of_time_at_location_mos" class="form-control" value="{{ $application->length_of_time_at_location_mos }}" placeholder="mos">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Year Business Established -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Year Business Established:</label>
                <input type="text" name="year_business_established" class="form-control" value="{{ $application->year_business_established }}" placeholder="year business established">
            </div>
        </div>
    </div>
</fieldset>
