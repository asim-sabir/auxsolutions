<fieldset>
    <!-- corporate form -->
    <div class="row">
        <div class="col-md-6">
            <!-- legal name -->
            <div class="form-group">
                <label>Corporate/Legal Name(Merchant):</label>
                <input type="text" name="corporate_name" class="form-control" value="{{ $application->corporate_name }}" placeholder="corporate/legal name">
            </div>
            <!-- corporate address -->
            <div class="form-group">
                <label>Corporate Address:</label>
                <input type="text" name="corporate_address" class="form-control" value="{{ $application->corporate_address }}" placeholder="corporate address">
            </div>
            <!-- corporate city -->
            <div class="form-group">
                <label>City:</label>
                <input type="text" name="corporate_city" class="form-control" value="{{ $application->corporate_city }}" placeholder="city">
            </div>
            <!-- corporate state -->
            <div class="form-group">
                <label>State:</label>
                <input type="text" name="corporate_state" class="form-control" value="{{ $application->corporate_state }}" placeholder="state">
            </div>
            <!-- corporate zip -->
            <div class="form-group">
                <label>Zip:</label>
                <input type="text" name="corporate_zip" class="form-control" value="{{ $application->corporate_zip }}" placeholder="zip">
            </div>
            <!-- corporate Phone no -->
            <div class="form-group">
                <label>Phone No:</label>
                <input type="text" name="corporate_phone_no" class="form-control" value="{{ $application->corporate_phone_no }}" placeholder="phone no">
            </div>
            <!-- corporate fax no -->
            <div class="form-group">
                <label>Fax No:</label>
                <input type="text" name="corporate_fax_no" class="form-control" value="{{ $application->corporate_fax_no }}" placeholder="fax no">
            </div>
            <!-- corporate Federal Tex Id -->
            <div class="form-group">
                <label>Federal Tex Id:</label>
                <input type="text" name="corporate_federal_tex_id" class="form-control" value="{{ $application->corporate_federal_tex_id }}" placeholder="federal tex id">
            </div>
            <!-- corporate contact person -->
            <div class="form-group">
                <label>Contact Person:</label>
                <input type="text" name="corporate_contact_person" class="form-control" value="{{ $application->corporate_contact_person }}" placeholder="contact person">
            </div>
        </div>
        <!-- business form -->
        <div class="col-md-6">
            <!-- doing business as -->
            <div class="form-group">
                <label>Doing Business As:</label>
                <input type="text" name="doing_business_as" class="form-control" value="{{ $application->doing_business_as }}" placeholder="doing business as">
            </div>
            <!-- Business address -->
            <div class="form-group">
                <label>Location Address:</label>
                <input type="text" name="business_address" class="form-control" value="{{ $application->business_address }}" placeholder="Location address">
            </div>
            <!-- business city -->
            <div class="form-group">
                <label>City:</label>
                <input type="text" name="business_city" class="form-control" value="{{ $application->business_city }}" placeholder="city">
            </div>
            <!-- business state -->
            <div class="form-group">
                <label>State:</label>
                <input type="text" name="business_state" class="form-control" value="{{ $application->business_state }}" placeholder="state">
            </div>
            <!-- business zip -->
            <div class="form-group">
                <label>Zip:</label>
                <input type="text" name="business_zip" class="form-control" value="{{ $application->business_zip }}" placeholder="zip">
            </div>
            <!-- business Phone no -->
            <div class="form-group">
                <label>Phone No:</label>
                <input type="text" name="business_phone_no_1" class="form-control" value="{{ $application->business_phone_no_1 }}" placeholder="phone no">
            </div>
            <!-- business Alternate no -->
            <div class="form-group">
                <label>Alternate No:</label>
                <input type="text" name="business_phone_no_2" class="form-control" value="{{ $application->business_phone_no_2 }}" placeholder="Alternate no">
            </div>
            <!-- business Email -->
            <div class="form-group">
                <label>Email address:</label>
                <input type="email" name="business_email" class="form-control" value="{{ $application->business_email }}" placeholder="your@email.com">
            </div>
        </div>
    </div>
</fieldset>
