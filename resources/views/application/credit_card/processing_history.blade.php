<fieldset>
    <div class="row">
        <!-- Is your business a licensed medical cannabis dispensary?-->
        <div class="col-md-6">
            <div class="form-group">
                <label>Is your business a licensed medical cannabis dispensary?:</label>
                <div class="col-lg-12">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="is_licensed_medical_cannabis" data-fouc value="1" {{ $application->is_licensed_medical_cannabis == 1 ? 'checked' : '' }}>
                            Yes
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="is_licensed_medical_cannabis" data-fouc value="0" {{ $application->is_licensed_medical_cannabis == 0 ? 'checked' : '' }}>
                            No
                        </label>
                    </div>
                    </div>
                <input type="text" name="is_licensed_medical_cannabis_detail" class="form-control" value="{{ $application->is_licensed_medical_cannabis_detail }}" placeholder="If YES, please provide">
            </div>
        </div>
        <!-- Date original license was granted-->
        <div class="col-md-6">
            <div class="form-group">
                <label>Date original license was granted:</label>
                <div class="col-lg-12">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="date_of_original_licence_granted" data-fouc value="1" {{ $application->date_of_original_licence_granted == 1 ? 'checked' : '' }}>
                            Yes
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="date_of_original_licence_granted" data-fouc value="0" {{ $application->date_of_original_licence_granted == 0 ? 'checked' : '' }}>
                            No
                        </label>
                    </div>
                    </div>
                <input type="text" name="date_of_original_licence_granted_detail" class="form-control" value="{{ $application->date_of_original_licence_granted_detail }}" placeholder="">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- License no-->
        <div class="col-md-6">
            <div class="form-group">
                <label>License No:</label>
                <div class="col-lg-12">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="license_no" data-fouc value="1" {{ $application->license_no == 1 ? 'checked' : '' }}>
                            Yes
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="license_no" data-fouc value="0" {{ $application->license_no == 0 ? 'checked' : '' }}>
                            No
                        </label>
                    </div>
                    </div>
                <input type="text" name="license_no_detail" class="form-control" value="{{ $application->license_no_detail }}" placeholder="license no">
            </div>
        </div>
        <!-- License Expiration Date-->
        <div class="col-md-6">
            <div class="form-group">
                <label>License Expiration Date:</label>
                <div class="col-lg-12">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="license_expiration_date" data-fouc value="1" {{ $application->license_expiration_date == 1 ? 'checked' : ''  }}>
                            Yes
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="license_expiration_date" data-fouc value="0" {{ $application->license_expiration_date == 0 ? 'checked' : ''  }}>
                            No
                        </label>
                    </div>
                    </div>
                <input type="text" name="license_expiration_date_detail" class="form-control" value="{{ $application->license_expiration_date_detail }}" placeholder="">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Has your business ever been denied a license or had its license terminated, suspended, or revoked for cause? -->
        <div class="col-md-12">
            <div class="form-group">
                <label>Has your business ever been denied a license or had its license terminated, suspended, or revoked for cause?:</label>
                <div class="col-lg-12">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="licence_terminated" data-fouc value="1" {{ $application->licence_terminated == 1 ? 'checked' : '' }}>
                            Yes
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="licence_terminated" data-fouc value="0" {{ $application->licence_terminated == 0 ? 'checked' : '' }}>
                            No
                        </label>
                    </div>
                </div>
                <input type="text" name="licence_terminated_detail" class="form-control" value="{{ $application->licence_terminated_detail }}" placeholder="If “Yes”, please explain">
            </div>
        </div>
    </div>
</fieldset>
