<fieldset>
    <div class="row" id="fee_schedule_section">
        <!-- A -->
        <div class="form-group">
            <label class="col-lg-12 col-form-label"><b>A: One-Time Equipment Set-up and Implementation and Training Fees:</b></label>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" disabled readonly data-fouc value="1"
                               {{ $application->one_time_equipment_set_up == 1 ? 'checked' : '' }}>
                        <span>
                            One Time Fee of $ <a href="javascript:;" class="fee-schedule-form-payment">{{ $application->one_time_equipment_set_up_amount? $application->one_time_equipment_set_up_amount : '0.00' }}</a> plus shipping for one (1) Merchant Terminal or Scanner (plus $ <a href="javascript:;">{{ $application->additional_merchant_terminal_amount? $application->additional_merchant_terminal_amount : '0.00' }}</a> for each additional Merchant Terminal or Scanner) for implementation, initial terminal set-up and coding and training
                        </span>
                    </label>
                </div>
            </div>
        </div>

        <!-- B -->
        <div class="form-group">
            <label class="col-lg-12 col-form-label"><b>B: Sales Volume Fee:</b></label>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" disabled readonly data-fouc value="1" {{ $application->sale_volume_fee == 1 ? 'checked' : '' }}>
                        <span>
                            Retail and Online: <a href="javascript:;">{{ $application->sale_volume_fee_amount? $application->sale_volume_fee_amount : '0.00' }}</a> % of Gross Transaction amount (“Merchant Discount Rate”) + $ <a href="javascript:;">{{ $application->per_transaction_amount? $application->per_transaction_amount : '0.00' }}</a> per Transaction + pass-through of all third-party authorization, transaction, and settlement fees
                        </span>
                    </label>
                </div>
            </div>
        </div>

        <!-- C -->
        <div class="form-group">
            <label class="col-lg-12 col-form-label"><b>C: Chargeback and Item Processing Fees::</b></label>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" disabled readonly data-fouc value="1" {{ $application->charge_back == 1 ? 'checked' : '' }}>
                        <span>
                            Fees of $ <a href="javascript:;">{{ $application->charge_back_amount? $application->charge_back_amount : '0.00' }}</a> per chargeback and $ <a href="javascript:;">{{ $application->per_returned_ach_item_amount? $application->per_returned_ach_item_amount : '0.00' }}</a> per returned ACH item shall apply to Merchant
                        </span>
                    </label>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" disabled readonly data-fouc value="1" {{ $application->limit_charge_back == 1 ? 'checked' : '' }}>
                        <span>
                            Limit on Chargebacks and customer disputes: <a href="javascript:;">{{ $application->limit_charge_back_amount? $application->limit_charge_back_amount : '0.00'  }}</a>
                        </span>
                    </label>
                </div>
            </div>
        </div>

        <!-- Section 2 -->
        <div class="form-group">
            <label class="col-lg-12 col-form-label"><b>Reserve Account and Bonding Requirements:</b></label>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" disabled readonly data-fouc value="1" {{ $application->reserve_account == 1 ? 'checked' : '' }}>
                        <span>
                            Merchant shall establish a Reserve Account at Bank which shall be equal to $ <a href="javascript:;">{{ $application->reserve_account_amount? $application->reserve_account_amount : '0.00' }}</a> plus <a href="javascript:;">{{ $application->gross_transaction_volume? $application->gross_transaction_volume : '0.00' }}</a> %
                        </span>
                    </label>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" disabled readonly data-fouc value="1" {{ $application->bounding_amount == 1 ? 'checked' : '' }}>
                        <span>
                            Merchant shall post a Bond equal to $ <a href="javascript:;">{{ $application->bounding_amount_amount? $application->bounding_amount_amount : '0.00' }}</a> and designate Bank as beneficiary.
                        </span>
                    </label>
                </div>
            </div>
        </div>
    </div>
</fieldset>
