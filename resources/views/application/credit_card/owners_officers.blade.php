<fieldset>
    <div class="row">
        <!-- list 1-->
        <div class="col-md-6">
            <!-- name_1 -->
            <div class="form-group">
                <label>Name (1):</label>
                <input type="text" name="name_1" class="form-control" value="{{ $application->name_1 }}" placeholder="name 1">
            </div>
            <!-- title -->
            <div class="form-group">
                <label>Title:</label>
                <input type="text" name="title_1" class="form-control" value="{{ $application->title_1 }}" placeholder="title">
            </div>
            <!-- Residential Address -->
            <div class="form-group">
                <label>Residential Address:</label>
                <input type="text" name="residential_address_1" class="form-control" value="{{ $application->residential_address_1 }}" placeholder="residential address">
            </div>
            <!-- city -->
            <div class="form-group">
                <label>City:</label>
                <input type="text" name="residential_city_1" class="form-control" value="{{ $application->residential_city_1 }}" placeholder="city">
            </div>
            <!-- state -->
            <div class="form-group">
                <label>State:</label>
                <input type="text" name="residential_state_1" class="form-control" value="{{ $application->residential_state_1 }}" placeholder="state">
            </div>
            <!-- zip -->
            <div class="form-group">
                <label>Zip:</label>
                <input type="text" name="residential_zip_1" class="form-control" value="{{ $application->residential_zip_1 }}" placeholder="Zip">
            </div>
            <!-- Driver’s License Number  -->
            <div class="form-group">
                <label>Driver’s License Number:</label>
                <input type="text" name="driver_license_number_1" class="form-control" value="{{ $application->driver_license_number_1 }}" placeholder="driver license number">
            </div>
            <!-- SSN -->
            <div class="form-group">
                <label>SSN:</label>
                <input type="text" name="ssn_1" class="form-control" value="{{ $application->ssn_1 }}" placeholder="ssn">
            </div>
            <!-- Equity Ownership -->
            <div class="form-group">
                <label>Equity Ownership (%):</label>
                <input type="text" name="equity_ownership_1" class="form-control" value="{{ $application->equity_ownership_1 }}" placeholder="equity ownership">
            </div>
            <!-- Time at Residence -->
            <div class="form-group">
                <label>Time at Residence:</label>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="time_at_residence_1_yrs" class="form-control" value="{{ $application->time_at_residence_1_yrs }}" placeholder="yrs">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="time_at_residence_1_mos" class="form-control" value="{{ $application->time_at_residence_1_mos }}" placeholder="mos">
                        </div>
                    </div>
                </div>
            </div>
            <!-- residence type -->
            <div class="form-group">
                <label class="col-lg-3 col-form-label">Type:</label>
                <div class="col-lg-12">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-input-styled" name="residence_type_own_1" data-fouc value="1" {{ $application->residence_type_own_1 == 1 ? 'checked' : '' }}>
                            Own
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-input-styled" name="residence_type_rent_1" data-fouc value="1" {{ $application->residence_type_rent_1 == 1 ? 'checked' : '' }}>
                            Rent
                        </label>
                    </div>
                </div>
            </div>
            <!-- Date of Birth -->
            <div class="form-group">
                <label>Date of Birth:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-calendar22"></i></span>
                    </span>
                    <input type="text" name="date_of_birth_1" class="form-control daterange-single" value="{{ $application->date_of_birth_1 }}">
                </div>
            </div>
            <!-- Residence Telephone -->
            <div class="form-group">
                <label>Residence Telephone:</label>
                <input type="text" name="residence_telephone_1" class="form-control" value="{{ $application->residence_telephone_1 }}" placeholder="residence telephone">
            </div>
        </div>
        <!-- list 2-->
        <div class="col-md-6">
            <!-- name_1 -->
            <div class="form-group">
                <label>Name (2):</label>
                <input type="text" name="name_2" class="form-control" value="{{ $application->name_2 }}" placeholder="name 2">
            </div>
            <!-- title -->
            <div class="form-group">
                <label>Title:</label>
                <input type="text" name="title_2" class="form-control" value="{{ $application->title_2 }}" placeholder="title">
            </div>
            <!-- Residential Address -->
            <div class="form-group">
                <label>Residential Address:</label>
                <input type="text" name="residential_address_2" class="form-control" value="{{ $application->residential_address_2 }}" placeholder="residential address">
            </div>
            <!-- city -->
            <div class="form-group">
                <label>City:</label>
                <input type="text" name="residential_city_2" class="form-control" value="{{ $application->residential_city_2 }}" placeholder="city">
            </div>
            <!-- state -->
            <div class="form-group">
                <label>State:</label>
                <input type="text" name="residential_state_2" class="form-control" value="{{ $application->residential_state_2 }}" placeholder="state">
            </div>
            <!-- zip -->
            <div class="form-group">
                <label>Zip:</label>
                <input type="text" name="residential_zip_2" class="form-control" value="{{ $application->residential_zip_2 }}" placeholder="Zip">
            </div>
            <!-- Driver’s License Number  -->
            <div class="form-group">
                <label>Driver’s License Number:</label>
                <input type="text" name="driver_license_number_2" class="form-control" value="{{ $application->driver_license_number_2 }}" placeholder="driver license number">
            </div>
            <!-- SSN -->
            <div class="form-group">
                <label>SSN:</label>
                <input type="text" name="ssn_2 " class="form-control" value="{{ $application->ssn_2 }}" placeholder="ssn">
            </div>
            <!-- Equity Ownership -->
            <div class="form-group">
                <label>Equity Ownership (%):</label>
                <input type="text" name="equity_ownership_2 " class="form-control" value="{{ $application->equity_ownership_2 }}" placeholder="equity ownership 2">
            </div>
            <!-- Time at Residence -->
            <div class="form-group">
                <label>Time at Residence:</label>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="time_at_residence_2_yrs" class="form-control" value="{{ $application->time_at_residence_2_yrs }}" placeholder="yrs">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="time_at_residence_2_mos" class="form-control" value="{{ $application->time_at_residence_2_mos }}" placeholder="mos">
                        </div>
                    </div>
                </div>
            </div>
            <!-- residence type -->
            <div class="form-group">
                <label class="col-lg-3 col-form-label">Type:</label>
                <div class="col-lg-12">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-input-styled" name="residence_type_own_2" value="1" data-fouc {{ $application->residence_type_own_2 == 1 ? 'checked' : '' }}>
                            Own
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-input-styled" name="residence_type_rent_2" value="1" data-fouc {{ $application->residence_type_rent_2 == 1 ? 'checked' : '' }}>
                            Rent
                        </label>
                    </div>
                </div>
            </div>
            <!-- Date of Birth -->
            <div class="form-group">
                <label>Date of Birth:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-calendar22"></i></span>
                    </span>
                    <input type="text" name="date_of_birth_2" class="form-control daterange-single" value="{{ $application->date_of_birth_2 }}">
                </div>
            </div>
            <!-- Residence Telephone -->
            <div class="form-group">
                <label>Residence Telephone:</label>
                <input type="text" name="residence_telephone_2" class="form-control" value="{{ $application->residence_telephone_2 }}" placeholder="residence telephone">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- BANK REFERENCE -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Bank Reference:</label>
                <input type="text" name="bank_reference" class="form-control" value="{{ $application->bank_reference }}" placeholder="bank reference">
            </div>
        </div>
        <!-- Account No -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Account No:</label>
                <input type="text" name="bank_reference_account_no" class="form-control" value="{{ $application->bank_reference_account_no }}" placeholder="account no">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- telephone no -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Telephone No:</label>
                <input type="text" name="bank_reference_telephone_no" class="form-control" value="{{ $application->bank_reference_telephone_no }}" placeholder="telephone no">
            </div>
        </div>
        <!-- Contact -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Contact:</label>
                <input type="text" name="bank_reference_contact" class="form-control" value="{{ $application->bank_reference_contact }}" placeholder="contact">
            </div>
        </div>
    </div>

    <!-- Trade Reference 1-->
    <div class="row">
        <!-- Trade Reference -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Trade Reference (1):</label>
                <input type="text" name="trade_reference_1" class="form-control" value="{{ $application->trade_reference_1 }}" placeholder="trade reference">
            </div>
        </div>
        <!-- Account No -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Account No:</label>
                <input type="text" name="trade_reference_1_account_no" class="form-control" value="{{ $application->trade_reference_1_account_no }}" placeholder="account no">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- telephone no -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Telephone No:</label>
                <input type="text" name="trade_reference_1_telephone_no" class="form-control" value="{{ $application->trade_reference_1_telephone_no }}" placeholder="telephone no">
            </div>
        </div>
        <!-- Contact -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Contact:</label>
                <input type="text" name="trade_reference_1_contact" class="form-control" value="{{ $application->trade_reference_1_contact }}" placeholder="contact">
            </div>
        </div>
    </div>

    <!-- Trade Reference 2-->
    <div class="row">
        <!-- Trade Reference -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Trade Reference (2):</label>
                <input type="text" name="trade_reference_2" class="form-control" value="{{ $application->trade_reference_2 }}" placeholder="trade reference">
            </div>
        </div>
        <!-- Account No -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Account No:</label>
                <input type="text" name="trade_reference_2_account_no" class="form-control" value="{{ $application->trade_reference_2_account_no }}" placeholder="account no">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- telephone no -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Telephone No:</label>
                <input type="text" name="trade_reference_2_telephone_no" class="form-control" value="{{ $application->trade_reference_2_telephone_no }}" placeholder="telephone no">
            </div>
        </div>
        <!-- Contact -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Contact:</label>
                <input type="text" name="trade_reference_2_contact" class="form-control" value="{{ $application->trade_reference_2_contact }}" placeholder="contact">
            </div>
        </div>
    </div>

    <!-- Trade Reference 3-->
    <div class="row">
        <!-- Trade Reference -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Trade Reference (3):</label>
                <input type="text" name="trade_reference_3" class="form-control" value="{{ $application->trade_reference_3 }}" placeholder="trade reference">
            </div>
        </div>
        <!-- Account No -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Account No:</label>
                <input type="text" name="trade_reference_3_account_no" class="form-control" value="{{ $application->trade_reference_3_account_no }}" placeholder="account no">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- telephone no -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Telephone No:</label>
                <input type="text" name="trade_reference_3_telephone_no" class="form-control" value="{{ $application->trade_reference_3_telephone_no }}" placeholder="telephone no">
            </div>
        </div>
        <!-- Contact -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Contact:</label>
                <input type="text" name="trade_reference_3_contact" class="form-control" value="{{ $application->trade_reference_3_contact }}" placeholder="contact">
            </div>
        </div>
    </div>

</fieldset>
