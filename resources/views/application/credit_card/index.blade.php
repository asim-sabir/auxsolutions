<div class="row">
    <div class="col-md-12">
        <div class="card" style="border: none !important;box-shadow: none;!important;">
            <form class="wizard-form credit-card-steps-basic" data-fouc id="credit_card_form" action="{{ auth()->guard('admin')->check() ? route('admin.application.store') : route('panel.application.store') }}">
                <input type="hidden" name="id" value="{{ $application_no }}">
                <h6>Business Name</h6>
                @include('application.credit_card.business_name')

                <h6>Merchant Profile</h6>
                @include('application.credit_card.merchant_profile')

                <h6>Processing History</h6>
                @include('application.credit_card.processing_history')

                <h6>Owners & Officers</h6>
                @include('application.credit_card.owners_officers')

                <h6>Form W-9</h6>
                @include('application.credit_card.w_9')
                @if(auth()->check())
                    @if(auth()->user()->user_type == \App\HelperModules\Constants::CUSTOMER && $application->fee_schedule_status == 1)
                        <h6>Fee Schedule</h6>
                        @include('application.credit_card.edit.fee_schedule')
                    @elseif(auth()->user()->user_type == \App\HelperModules\Constants::CUSTOMER)
                        <h6>Fee Schedule</h6>
                        @include('application.credit_card.fee_schedule')
                    @endif
                @elseif(auth()->guard('admin')->check())
                    <h6>Fee Schedule</h6>
                    @include('application.credit_card.fee_schedule')
                @endif
            </form>
        </div>
    </div>
</div>
