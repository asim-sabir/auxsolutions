<fieldset>
    <input type="hidden" name="one_time_equipment_set_up_amount" id="fee-schedule-form-payment-1" value="{{ $application->one_time_equipment_set_up_amount ? $application->one_time_equipment_set_up_amount : '' }}">
    <input type="hidden" name="additional_merchant_terminal_amount" id="fee-schedule-form-payment-2" value="{{ $application->additional_merchant_terminal_amount ? $application->additional_merchant_terminal_amount : '' }}">
    <input type="hidden" name="sale_volume_fee_amount" id="fee-schedule-form-payment-3" value="{{ $application->sale_volume_fee_amount ? $application->sale_volume_fee_amount : '' }}">
    <input type="hidden" name="per_transaction_amount" id="fee-schedule-form-payment-4" value="{{ $application->per_transaction_amount ? $application->per_transaction_amount : '' }}">
    <input type="hidden" name="charge_back_amount" id="fee-schedule-form-payment-5" value="{{ $application->charge_back_amount ? $application->charge_back_amount : '' }}">
    <input type="hidden" name="per_returned_ach_item_amount" id="fee-schedule-form-payment-6" value="{{ $application->per_returned_ach_item_amount ? $application->per_returned_ach_item_amount : '' }}">
    <input type="hidden" name="limit_charge_back_amount" id="fee-schedule-form-payment-7" value="{{ $application->limit_charge_back_amount ? $application->limit_charge_back_amount : '' }}">
    <input type="hidden" name="reserve_account_amount" id="fee-schedule-form-payment-8" value="{{ $application->reserve_account_amount ? $application->reserve_account_amount : '' }}">
    <input type="hidden" name="gross_transaction_volume" id="fee-schedule-form-payment-9" value="{{ $application->gross_transaction_volume ? $application->gross_transaction_volume : '' }}">
    <input type="hidden" name="bounding_amount_amount" id="fee-schedule-form-payment-10" value="{{ $application->bounding_amount_amount ? $application->bounding_amount_amount : '' }}">

    <div class="form-check form-check-switch form-check-switch-left text-right">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input form-check-input-switch" data-on-text="Enable" data-off-text="Disable" name="fee_schedule_status" value="1" {{ $application->fee_schedule_status == 1 ? 'checked' : '' }}>
        </label>
    </div>
    <div class="row" id="fee_schedule_section">
        <!-- A -->
        <div class="form-group">
            <label class="col-lg-12 col-form-label"><b>A: One-Time Equipment Set-up and Implementation and Training Fees:</b></label>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" name="one_time_equipment_set_up" data-fouc value="1"
                               {{ $application->one_time_equipment_set_up == 1 ? 'checked' : '' }}>
                        <span>
                            One Time Fee of $ <a href="#" data-toggle="modal" data-target="#fee_schedule_form_values" class="fee-schedule-form-payment fee-schedule-form-payment-1" data-title="One-Time Equipment Set-up and Implementation and Training Fees" data-inputid="fee-schedule-form-payment-1" data-class="fee-schedule-form-payment-1">{{ $application->one_time_equipment_set_up_amount? $application->one_time_equipment_set_up_amount : '0.00' }}</a> plus shipping for one (1) Merchant Terminal or Scanner (plus $ <a href="#" data-toggle="modal" data-target="#fee_schedule_form_values" class="fee-schedule-form-payment fee-schedule-form-payment-2" data-title="Additional Merchant Terminal or Scanner Fee" data-inputid="fee-schedule-form-payment-2" data-class="fee-schedule-form-payment-2">{{ $application->additional_merchant_terminal_amount? $application->additional_merchant_terminal_amount : '0.00' }}</a> for each additional Merchant Terminal or Scanner) for implementation, initial terminal set-up and coding and training
                        </span>
                    </label>
                </div>
            </div>
        </div>

        <!-- B -->
        <div class="form-group">
            <label class="col-lg-12 col-form-label"><b>B: Sales Volume Fee:</b></label>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" name="sale_volume_fee" data-fouc value="1" {{ $application->sale_volume_fee == 1 ? 'checked' : '' }}>
                        <span>
                            Retail and Online: <a href="#" data-toggle="modal" data-target="#fee_schedule_form_values" class="fee-schedule-form-payment fee-schedule-form-payment-3" data-title="Sales Volume Fee" data-inputid="fee-schedule-form-payment-3" data-class="fee-schedule-form-payment-3">{{ $application->sale_volume_fee_amount? $application->sale_volume_fee_amount : '0.00' }}</a> % of Gross Transaction amount (“Merchant Discount Rate”) + $ <a href="#" data-toggle="modal" data-target="#fee_schedule_form_values" class="fee-schedule-form-payment fee-schedule-form-payment-4" data-title="Per Transaction Fee" data-inputid="fee-schedule-form-payment-4" data-class="fee-schedule-form-payment-4">{{ $application->per_transaction_amount? $application->per_transaction_amount : '0.00' }}</a> per Transaction + pass-through of all third-party authorization, transaction, and settlement fees
                        </span>
                    </label>
                </div>
            </div>
        </div>

        <!-- C -->
        <div class="form-group">
            <label class="col-lg-12 col-form-label"><b>C: Chargeback and Item Processing Fees::</b></label>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" name="charge_back" data-fouc value="1" {{ $application->charge_back == 1 ? 'checked' : '' }}>
                        <span>
                            Fees of $ <a href="#" data-toggle="modal" data-target="#fee_schedule_form_values" class="fee-schedule-form-payment fee-schedule-form-payment-5" data-title="Chargeback and Item Processing Fees" data-inputid="fee-schedule-form-payment-5" data-class="fee-schedule-form-payment-5">{{ $application->charge_back_amount? $application->charge_back_amount : '0.00' }}</a> per chargeback and $ <a href="#" data-toggle="modal" data-target="#fee_schedule_form_values" class="fee-schedule-form-payment fee-schedule-form-payment-6" data-title="Per Returned ACH Item Fee" data-inputid="fee-schedule-form-payment-6" data-class="fee-schedule-form-payment-6">{{ $application->per_returned_ach_item_amount? $application->per_returned_ach_item_amount : '0.00' }}</a> per returned ACH item shall apply to Merchant
                        </span>
                    </label>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" name="limit_charge_back" data-fouc value="1" {{ $application->limit_charge_back == 1 ? 'checked' : '' }}>
                        <span>
                            Limit on Chargebacks and customer disputes: <a href="#" data-toggle="modal" data-target="#fee_schedule_form_values" class="fee-schedule-form-payment fee-schedule-form-payment-7" data-title="Limit on Chargebacks and customer disputes" data-inputid="fee-schedule-form-payment-7" data-class="fee-schedule-form-payment-7">{{ $application->limit_charge_back_amount? $application->limit_charge_back_amount : '0.00' }}</a>
                        </span>
                    </label>
                </div>
            </div>
        </div>

        <!-- Section 2 -->
        <div class="form-group">
            <label class="col-lg-12 col-form-label"><b>Reserve Account and Bonding Requirements:</b></label>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" name="reserve_account" data-fouc value="1" {{ $application->reserve_account == 1 ? 'checked' : '' }}>
                        <span>
                            Merchant shall establish a Reserve Account at Bank which shall be equal to $ <a href="#" data-toggle="modal" data-target="#fee_schedule_form_values" class="fee-schedule-form-payment fee-schedule-form-payment-8" data-title="Reserve Account" data-inputid="fee-schedule-form-payment-8" data-class="fee-schedule-form-payment-8">{{ $application->reserve_account_amount? $application->reserve_account_amount : '0.00' }}</a> plus <a href="#" data-toggle="modal" data-target="#fee_schedule_form_values" class="fee-schedule-form-payment fee-schedule-form-payment-9" data-title="Monthly Gross Transaction Volume" data-inputid="fee-schedule-form-payment-9" data-class="fee-schedule-form-payment-9">{{ $application->gross_transaction_volume? $application->gross_transaction_volume : '0.00' }}</a> %
                        </span>
                    </label>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-input-styled" name="bounding_amount" data-fouc value="1" {{ $application->bounding_amount == 1 ? 'checked' : '' }}>
                        <span>
                            Merchant shall post a Bond equal to $ <a href="#" data-toggle="modal" data-target="#fee_schedule_form_values" class="fee-schedule-form-payment fee-schedule-form-payment-10" data-title="Bounding Amount" data-inputid="fee-schedule-form-payment-10" data-class="fee-schedule-form-payment-10">{{ $application->bounding_amount_amount? $application->bounding_amount_amount : '0.00' }}</a> and designate Bank as beneficiary.
                        </span>
                    </label>
                </div>
            </div>
        </div>
    </div>
</fieldset>
