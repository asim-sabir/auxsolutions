<fieldset>
    <div class="row">
        <!-- Name (as shown on your income tax return) -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Name (as shown on your income tax return):</label>
                <input type="text" name="form_w9_name" class="form-control" value="{{ $application ? $application->form_w9_name : '' }}" placeholder="name">
            </div>
        </div>
        <!-- Business name/disregarded entity name, if different from above -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Business name/disregarded entity name, if different from above:</label>
                <input type="text" name="form_w9_business_name" class="form-control" value="{{ $application ? $application->form_w9_business_name : '' }}" placeholder="business name">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Address (number, street, and apt. or suite no.) -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Address (number, street, and apt. or suite no.) :</label>
                <input type="text" name="form_w9_address" class="form-control" value="{{ $application ? $application->form_w9_address : '' }}" placeholder="address">
            </div>
        </div>
        <!-- city -->
        <div class="col-md-6">
            <div class="form-group">
                <label>City:</label>
                <input type="text" name="form_w9_city" class="form-control" value="{{ $application ? $application->form_w9_city : '' }}" placeholder="city">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- State -->
        <div class="col-md-6">
            <div class="form-group">
                <label>State :</label>
                <input type="text" name="form_w9_state" class="form-control" value="{{ $application ? $application->form_w9_state : '' }}" placeholder="state">
            </div>
        </div>
        <!-- Zip Code -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Zip Code:</label>
                <input type="text" name="form_w9_zip_code" class="form-control" value="{{ $application ? $application->form_w9_zip_code : '' }}" placeholder="zip code">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Requester’s name (optional)  -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Requester’s Name (optional):</label>
                <input type="text" name="form_w9_requester_name" class="form-control" value="{{ $application ? $application->form_w9_requester_name : '' }}" placeholder="requester name">
            </div>
        </div>
        <!-- Requester’s address (optional)  -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Requester’s Address (optional):</label>
                <input type="text" name="form_w9_requester_address" class="form-control" value="{{ $application ? $application->form_w9_requester_address : '' }}" placeholder="requester address">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- List account number(s) here (optional)   -->
        <div class="col-md-6">
            <div class="form-group">
                <label>List account number(s) here (optional):</label>
                <input type="text" name="form_w9_account_no_list" class="form-control" value="{{ $application ? $application->form_w9_account_no_list  : ''}}" placeholder="account number(s)">
            </div>
        </div>
        <!-- Social security number -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Social Security Number:</label>
                <input type="text" name="social_security_number" class="form-control" value="{{ $application ? $application->social_security_number : '' }}" placeholder="social security number">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Employer identification number -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Employer Identification Number:</label>
                <input type="text" name="employer_identification_number" class="form-control" value="{{ $application ? $application->employer_identification_number : '' }}" placeholder="employer identification number">
            </div>
        </div>
    </div>
</fieldset>