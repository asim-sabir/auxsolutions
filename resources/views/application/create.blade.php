@extends('panel.layout.default', ['page' => 'Application# ' . $application_no])
@section('css')
    @parent
    <style>
        .wizard>.content{
            padding: 20px !important;
        }
        .form-control{
            height: 3.25003rem !important;
            border: 2px solid #ddd !important;
            border-radius: 0px !important;
        }
        .fee-schedule-form-payment{
            font-weight: bold;
            text-decoration: underline !important;
            font-size: 15px;
        }
    </style>
@endsection
@section('contents')
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="card">
                <div class="card-body">
                    @include('layouts.general.messages')
                    <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 float-right">
                        <li class="nav-item"><a href="#questionnaire" class="nav-link active" data-toggle="tab">Questionnaire</a></li>
                        <li class="nav-item"><a href="#credit_card" class="nav-link @if(!$application->questionnaire_form_status) disabled @endif" data-toggle="tab">Credit Card</a></li>
                        <li class="nav-item"><a href="#bank_account" class="nav-link @if(!$application->questionnaire_form_status) disabled @endif" data-toggle="tab">Bank Account</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="questionnaire">
                            @include('panel.application.questionnaire.index')
                        </div>
                        <div class="tab-pane fade" id="credit_card">
                            @if(!$application->questionnaire_form_status)
                                @include('panel.application.credit_card.index')
                            @endif
                        </div>
                        <div class="tab-pane fade" id="bank_account">
                            @if(!$application->questionnaire_form_status)
                                @include('panel.application.bank.index')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- set value -->
    <div id="fee_schedule_form_values" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="model-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body" style=" padding: 2.25rem !important;">
                    <div class="input-group">
                        <input type="text" class="form-control" id="fee-model-input">
                        <input type="hidden" class="form-control" id="fee-model-input-id-name">
                        <input type="hidden" class="form-control" id="fee-model-trigger-class-name">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- set value -->
@endsection
@section('scripts')
    @parent
    <script src="{{asset('js/plugins/forms/wizards/steps.min.js')}}"></script>
    <script src="{{asset('js/plugins/pickers/daterangepicker.js')}}"></script>
    <script>
        $(function () {
            sideBarToggle();
            dateRangeSingle();
            setInterval(function () {
                @if($application->questionnaire_form_status)
                    let form_url = '{{ route('panel.application.store') }}';
                    applicationAutoPostInfo(form_url, $('#credit_card_form').serialize());
                    applicationAutoPostInfo(form_url, $('#banking_form').serialize());
                @elseif(!$application->questionnaire_form_status)
                    form_url = '{{ route('panel.application.auto.store.questionnaire.form') }}';
                    applicationAutoPostInfo(form_url, $('#'));
                @endif
            }, 10000);
            $('#send-to-customer').click(function () {
                let url = '{{ route('panel.application.share') }}';
                let data = {
                    application_id: $('#application_no').val(),
                    email: $('#questionnaire_email').val()
                };
                shareApplication(url, data);
            })
        })
    </script>
    <script src="{{asset('js/pages/application.js')}}"></script>
@endsection
