@extends(auth()->guard('admin')->check() ? 'layouts.admin.default' : 'layouts.panel.default', ['page' => 'Application# ' . $application_no])
@section('css')
    @parent
    <style>
        .wizard>.content{
            padding: 20px !important;
        }
        .form-control{
            height: 3.25003rem !important;
            border: 2px solid #ddd !important;
            border-radius: 0px !important;
        }
        .fee-schedule-form-payment{
            font-weight: bold;
            text-decoration: underline !important;
            font-size: 15px;
        }
    </style>
@endsection
@section('contents')
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 float-right">
                        <li class="nav-item"><a href="#questionnaire" class="nav-link active" data-toggle="tab">Questionnaire</a></li>
                        @if(!$application->questionnaire_form_status || auth()->guard('admin')->check())
                            <li class="nav-item"><a href="#credit_card" class="nav-link" data-toggle="tab">Credit Card</a></li>
                            <li class="nav-item"><a href="#bank_account" class="nav-link" data-toggle="tab">Bank Account</a></li>
                        @endif
                    </ul>
                    <div class="clearfix"></div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="questionnaire">
                            <form method="post" action="{{ auth()->guard('admin')->check() ? route('admin.application.store.questionnaire.form') : route('panel.application.store.questionnaire.form') }}" enctype="multipart/form-data">
                                @csrf
                                @include('application.questionnaire.index')
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit Application</button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="credit_card">
                            @if(!$application->questionnaire_form_status || auth()->guard('admin')->check())
                                @include('application.credit_card.index')
                            @endif
                        </div>
                        <div class="tab-pane fade" id="bank_account">
                            @if(!$application->questionnaire_form_status || auth()->guard('admin')->check())
                                @include('application.bank.index')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{asset('js/plugins/forms/wizards/steps.min.js')}}"></script>
    <script src="{{asset('js/plugins/pickers/daterangepicker.js')}}"></script>
    <script src="{{asset('js/pages/application.js')}}"></script>
    <script>
        $(function () {
            sideBarToggle();
            dateRangeSingle();
        })
    </script>
@endsection
