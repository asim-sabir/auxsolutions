<input type="hidden" name="id" value="{{ $application_no }}" required id="application_no">
<div class="row">
    <div class="col-md-6">
        <!-- legal name -->
        <div class="form-group">
            <label>Legal Name Of Business:</label>
            <input type="text" name="questionnaire_business_name" class="form-control" value="{{ $application->questionnaire_business_name }}" required>
        </div>
        <!-- Merchant DBA -->
        <div class="form-group">
            <label>Merchant DBA:</label>
            <input type="text" name="questionnaire_merchant_dba" class="form-control" value="{{ $application->questionnaire_merchant_dba }}" required>
        </div>
        <!-- DBA address -->
        <div class="form-group">
            <label>DBA address:</label>
            <input type="text" name="questionnaire_dba_address" class="form-control" value="{{ $application->questionnaire_dba_address }}" required>
        </div>
        <!-- DBA city -->
        <div class="form-group">
            <label>DBA City:</label>
            <input type="text" name="questionnaire_dba_city" class="form-control" value="{{ $application->questionnaire_dba_city }}" required>
        </div>
        <!-- DBA state -->
        <div class="form-group">
            <label>DBA State:</label>
            <input type="text" name="questionnaire_dba_state" class="form-control" value="{{ $application->questionnaire_dba_state }}" required>
        </div>
        <!-- DBA zip -->
        <div class="form-group">
            <label>DBA Zip:</label>
            <input type="text" name="questionnaire_dba_zip" class="form-control" value="{{ $application->questionnaire_dba_zip }}" required>
        </div>
        <!-- Phone no -->
        <div class="form-group">
            <label>Business Tel No:</label>
            <input type="text" name="questionnaire_business_phone_no" class="form-control" value="{{ $application->questionnaire_business_phone_no }}" required>
        </div>
        <!-- Total Months in Business, (or NEW) -->
        <div class="form-group">
            <label>Total Months in Business, (or NEW):</label>
            <input type="text" name="questionnaire_total_business_months" class="form-control" value="{{ $application->questionnaire_total_business_months }}" required>
        </div>
    </div>

    <div class="col-md-6">
        <!-- merchant email -->
        <div class="form-group">
            <label>Merchant Email:</label>
            <input type="email" name="questionnaire_email" id="questionnaire_email" class="form-control" value="{{ $application->questionnaire_email }}" required>
        </div>
        <!-- Estimated Annual Sales -->
        <div class="form-group">
            <label>Estimated Annual Sales:</label>
            <input type="text" name="questionnaire_annual_sales" class="form-control" value="{{ $application->questionnaire_annual_sales }}" required>
        </div>
        <!-- Federal Tax ID (EIN) -->
        <div class="form-group">
            <label>Federal Tax ID (EIN):</label>
            <input type="text" name="questionnaire_tax_id" class="form-control" value="{{ $application->questionnaire_tax_id }}" required>
        </div>
        <!-- Type of Ownership (Sole P, Inc. or LLC) -->
        <div class="form-group">
            <label>Type of Ownership (Sole P, Inc. or LLC):</label>
            <input type="text" name="questionnaire_ownership_type" class="form-control" value="{{ $application->questionnaire_ownership_type }}" required>
        </div>
        <!-- Legal Address (if Different from DBA address) -->
        <div class="form-group">
            <label>Legal Address (if Different from DBA address):</label>
            <input type="text" name="questionnaire_legal_address" class="form-control" value="{{ $application->questionnaire_legal_address }}" required>
        </div>
        <!-- city -->
        <div class="form-group">
            <label>City:</label>
            <input type="text" name="questionnaire_legal_city" class="form-control" value="{{ $application->questionnaire_legal_city }}" required>
        </div>
        <!-- state -->
        <div class="form-group">
            <label>State:</label>
            <input type="text" name="questionnaire_legal_state" class="form-control" value="{{ $application->questionnaire_legal_state }}" required>
        </div>
        <!-- zip -->
        <div class="form-group">
            <label>Zip:</label>
            <input type="text" name="questionnaire_legal_zip" class="form-control" value="{{ $application->questionnaire_legal_zip }}" required>
        </div>
    </div>
</div>
<!-- reference -->
<div class="row">
    <!-- Owner/Principal 1 -->
    <div class="col-md-6">
        <fieldset>
            <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Owner/Principal 1</legend>
        </fieldset>
        <!-- Full Name  -->
        <div class="form-group">
            <label>Full Name (First, Middle, Last):</label>
            <input type="text" name="questionnaire_owner_1_name" class="form-control" value="{{ $application->questionnaire_owner_1_name }}" required>
        </div>
        <!-- Residential Address -->
        <div class="form-group">
            <label>Residential Address:</label>
            <input type="text" name="questionnaire_owner_1_address" class="form-control" value="{{ $application->questionnaire_owner_1_address }}" required>
        </div>
        <!-- city -->
        <div class="form-group">
            <label>City:</label>
            <input type="text" name="questionnaire_owner_1_city" class="form-control" value="{{ $application->questionnaire_owner_1_city }}" required>
        </div>
        <!-- state -->
        <div class="form-group">
            <label>State:</label>
            <input type="text" name="questionnaire_owner_1_state" class="form-control" value="{{ $application->questionnaire_owner_1_state }}" required>
        </div>
        <!-- zip -->
        <div class="form-group">
            <label>Zip:</label>
            <input type="text" name="questionnaire_owner_1_zip" class="form-control" value="{{ $application->questionnaire_owner_1_zip }}" required>
        </div>
        <!-- Ownership % -->
        <div class="form-group">
            <label>Ownership %:</label>
            <input type="text" name="questionnaire_owner_1_ownership_percentage" class="form-control" value="{{ $application->questionnaire_owner_1_ownership_percentage }}" required>
        </div>
        <!-- Birthday -->
        <div class="form-group">
            <label>Birthday:</label>
            <input type="text" name="questionnaire_owner_1_birthday" class="form-control daterange-single" value="{{ $application->questionnaire_owner_1_birthday }}" required>
        </div>
        <!-- Social Security # -->
        <div class="form-group">
            <label>Social Security #:</label>
            <input type="text" name="questionnaire_owner_1_social_security" class="form-control" value="{{ $application->questionnaire_owner_1_social_security }}" required>
        </div>
    </div>
    <!-- Owner/Principal 2 -->
    <div class="col-md-6">
        <fieldset>
            <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Owner/Principal 2</legend>
        </fieldset>
        <!-- Full Name  -->
        <div class="form-group">
            <label>Full Name (First, Middle, Last):</label>
            <input type="text" name="questionnaire_owner_2_name" class="form-control" value="{{ $application->questionnaire_owner_2_name }}">
        </div>
        <!-- Residential Address -->
        <div class="form-group">
            <label>Residential Address:</label>
            <input type="text" name="questionnaire_owner_2_address" class="form-control" value="{{ $application->questionnaire_owner_2_address }}">
        </div>
        <!-- city -->
        <div class="form-group">
            <label>City:</label>
            <input type="text" name="questionnaire_owner_2_city" class="form-control" value="{{ $application->questionnaire_owner_2_city }}">
        </div>
        <!-- state -->
        <div class="form-group">
            <label>State:</label>
            <input type="text" name="questionnaire_owner_2_state" class="form-control" value="{{ $application->questionnaire_owner_2_state }}">
        </div>
        <!-- zip -->
        <div class="form-group">
            <label>Zip:</label>
            <input type="text" name="questionnaire_owner_2_zip" class="form-control" value="{{ $application->questionnaire_owner_2_zip }}">
        </div>
        <!-- Ownership % -->
        <div class="form-group">
            <label>Ownership %:</label>
            <input type="text" name="questionnaire_owner_2_ownership_percentage" class="form-control" value="{{ $application->questionnaire_owner_2_ownership_percentage }}">
        </div>
        <!-- Birthday -->
        <div class="form-group">
            <label>Birthday:</label>
            <input type="text" name="questionnaire_owner_2_birthday" class="form-control daterange-single" value="{{ $application->questionnaire_owner_2_birthday }}">
        </div>
        <!-- Social Security # -->
        <div class="form-group">
            <label>Social Security #:</label>
            <input type="text" name="questionnaire_owner_2_social_security" class="form-control" value="{{ $application->questionnaire_owner_2_social_security }}">
        </div>
    </div>
</div>

<div class="row">
    <!-- Has the merchant previously submitted a merchant processing application? If so with whom? -->
    <div class="col-md-12">
        <div class="form-group">
            <label>Has the merchant previously submitted a merchant processing application? If so with whom?:</label>
            <input type="text" name="questionnaire_has_previous_application" class="form-control" value="{{ $application->questionnaire_has_previous_application }}">
        </div>
    </div>
</div>
<!-- licence -->
<div class="row">
    <!-- merchant licence -->
    @if($application->merchant_licence)
        <div class="form-group">
            <div class="media mt-0">
                <div class="mr-3">
                    <a href="#">
                        <img src="{{ asset('uploads/confidential/'. auth()->id() .'/'. $application->merchant_licence) }}" width="60" height="60" class="rounded-round" alt="">
                    </a>
                </div>
            </div>
        </div>
    @endif
    <div class="col-md-12">
        <div class="form-group">
            <label>Your Licence:</label>
            <input type="file" class="form-input-styled" data-fouc name="licence" required>
        </div>
    </div>
</div>
