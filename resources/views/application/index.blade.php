@extends('layouts.panel.default', ['page' => 'Application'])
@section('css')
    @parent
    <style>
        .form-control{
            height: 3.25003rem !important;
            border: 1px solid #ddd !important;
            border-radius: 0px !important;
        }
    </style>
@endsection
@section('contents')
    <div class="row">
        <div class="col-sm-6 col-xl-3">
            <div class="card card-body">
                <div class="media">
                    <div class="media-body">
                        <h3 class="font-weight-semibold mb-0">{{$applications->count()}} <span class="text-capitalize font-size-sm text-muted">Total</span></h3>
                    </div>
                    <div class="ml-3 align-self-center">
                        <i class="icon-copy icon-2x text-primary"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card card-body">
                <div class="media">
                    <div class="media-body">
                        <h3 class="font-weight-semibold mb-0">{{$applications->count()}} <span class="text-capitalize font-size-sm text-muted">New</span></h3>
                    </div>
                    <div class="ml-3 align-self-center">
                        <i class="icon-copy icon-2x text-primary"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card card-body">
                <div class="media">
                    <div class="media-body">
                        <h3 class="font-weight-semibold mb-0">0 <span class="text-capitalize font-size-sm text-muted">Pending</span></h3>
                    </div>
                    <div class="ml-3 align-self-center">
                        <i class="icon-briefcase3 icon-2x text-primary"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card card-body">
                <div class="media">
                    <div class="media-body">
                        <h3 class="font-weight-semibold mb-0">0 <span class="text-capitalize font-size-sm text-muted">Approved</span></h3>
                    </div>
                    <div class="ml-3 align-self-center">
                        <i class="icon-check icon-2x text-primary"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title font-weight-semibold"><i class="icon-file-presentation icon-1x mr-1"></i> Applications</h5>
            @if(auth()->user()->user_type == \App\HelperModules\Constants::MERCHANT)
                <div class="header-elements">
                    <a href="{{ route('panel.application.new', $new_application) }}" class="btn btn-dark">
                        <i class="icon-plus3 mr-2"></i>
                        New Application
                    </a>
                </div>
            @endif
        </div>
        <div class="card-body">
            <div class="table-responsive">
                @if(count($applications) > 0)
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Create Date</th>
                                <th>Corporate Name</th>
                                <th>Business Email</th>
                                <th>Phone No</th>
                                <th>Web Address</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($applications as $k => $v)
                                <tr>
                                    <td><span class="d-block">{{ $v->created_at->format('M j,Y') }} at {{ $v->created_at->format('h:i A') }}</span></td>
                                    <td><span class="d-block">{{  $v->corporate_name ? $v->corporate_name : 'not set' }}</span></td>
                                    <td><span class="d-block">{{ $v->business_email ? $v->business_email : 'not set' }}</span></td>
                                    <td><span class="d-block">{{ $v->corporate_phone_no ? $v->corporate_phone_no : 'not set' }}</span></td>
                                    <td><span class="d-block">{{ $v->web_address ? $v->web_address : 'not set' }}</span></td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    @if(auth()->user()->user_type == \App\HelperModules\Constants::MERCHANT)
                                                        <a href="#" class="dropdown-item"><i class="icon-file-text2"></i> Generate PDF</a>
                                                        <a href="#" class="dropdown-item"><i class="icon-pencil mr-2"></i> Create Adobe Sign</a>
                                                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#share-application-model" data-id="{{ $v->id  }}" id="share-application"><i class="icon-link mr-2"></i> Generate Link</a>
                                                    <a href="{{ route('panel.application.edit', $v->id) }}" class="dropdown-item"><i class="icon-pencil mr-2"></i> Edit</a>
                                                    @elseif(auth()->user()->user_type == \App\HelperModules\Constants::CUSTOMER)
                                                        <a href="{{ route('panel.application.edit.customer', $v->id) }}" class="dropdown-item"><i class="icon-pencil mr-2"></i> Edit</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <center> <h2> No Record Found </h2> </center>
                @endif
            </div>
        </div>
    </div>
    @if(count($applications) > 0)
        <div class="card">
            <div class="card-body">
                {{ $applications->appends(request()->query())->links() }}
            </div>
        </div>
    @endif
    <!-- generate link -->
    <div id="share-application-model" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Share Application</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style=" padding: 2.25rem !important;">
                    <form method="post">
                        @csrf
                        <input type="hidden" class="form-control" name="application_id" id="application-id">
                        <div class="input-group">
                            <input type="email" class="form-control" name="email">
                            <span class="input-group-append">
                                <button class="btn btn-light" type="button" id="send-application">Send</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- generate link -->
@endsection
@section('scripts')
    @parent
    <script>
        let form_url = '{{ route('panel.application.share') }}';
    </script>
    <script src="{{asset('js/pages/application.js')}}"></script>
@endsection
