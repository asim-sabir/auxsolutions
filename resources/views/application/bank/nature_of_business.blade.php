<fieldset>

    <div class="row">
        <!-- Products And Services Offered -->
        <div class="col-md-12">
            <div class="form-group">
                <label>Products And Services Offered:</label>
                <input type="text" name="bank_business_products" class="form-control" value="{{ $application->bank_business_products }}" placeholder="Products And Services Offered">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- States Of Operation -->
        <div class="col-md-12">
            <div class="form-group">
                <label>States Of Operation:</label>
                <input type="text" name="bank_business_operations" class="form-control" value="{{ $application->bank_business_operations }}" placeholder="States Of Operation">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Current Licenses-->
        <div class="col-md-12">
            <div class="form-group">
                <label>Current Licenses:</label>
                <input type="text" name="bank_business_current_licenses" class="form-control" value="{{ $application->bank_business_current_licenses }}" placeholder="Current Licenses">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Expected Monthly Sales-->
        <div class="col-md-12">
            <div class="form-group">
                <label>Expected Monthly Sales:</label>
                <input type="text" name="bank_business_monthly_sales" class="form-control" value="{{ $application->bank_business_monthly_sales }}" placeholder="Expected Monthly Sales">
            </div>
        </div>
    </div>
</fieldset>
