<fieldset>
    <div class="row">
        <!-- Applicant/Legal Name -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Applicant/Legal Name:</label>
                <input type="text" name="bank_applicant_legal_name" class="form-control" value="{{ $application->bank_applicant_legal_name }}" placeholder="Applicant/Legal Name">
            </div>
        </div>
        <!-- Title -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Title:</label>
                <input type="text" name="bank_applicant_title" class="form-control" value="{{ $application->bank_applicant_title }}" placeholder="Title">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- email -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Email:</label>
                <input type="email" name="bank_applicant_email" class="form-control" value="{{ $application->bank_applicant_email }}" placeholder="email">
            </div>
        </div>
        <!-- phone -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Phone:</label>
                <input type="text" name="bank_applicant_phone_no" class="form-control" value="{{ $application->bank_applicant_phone_no }}" placeholder="phone">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- business name -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Legal Name Of Business:</label>
                <input type="text" name="bank_business_name" class="form-control" value="{{ $application->bank_business_name }}" placeholder="business name">
            </div>
        </div>
        <!-- EIN -->
        <div class="col-md-6">
            <div class="form-group">
                <label>EIN:</label>
                <input type="text" name="bank_business_ein" class="form-control" value="{{ $application->bank_business_ein }}" placeholder="ein">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- registered ficitious name -->
        <div class="col-md-12">
            <div class="form-group">
                <label>Registered Ficitious Name:</label>
                <input type="text" name="bank_business_registered_ficitious_name" class="form-control" value="{{ $application->bank_business_registered_ficitious_name }}" placeholder="registered ficitious name">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- corporate address -->
        <div class="col-md-12">
            <div class="form-group">
                <label>Corporate Address:</label>
                <input type="text" name="bank_business_corporate_address" class="form-control" value="{{ $application->bank_business_corporate_address }}" placeholder="corporate address">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- corporate contact person -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Contact Phone:</label>
                <input type="text" name="bank_business_phone_no" class="form-control" value="{{ $application->bank_business_phone_no }}" placeholder="contact phone">
            </div>
        </div>
        <!-- preferred email -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Contact Phone:</label>
                <input type="text" name="bank_business_preferred_email" class="form-control" value="{{ $application->bank_business_preferred_email }}" placeholder="preferred email">
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Date Established -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Date Established:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-calendar22"></i></span>
                    </span>
                    <input type="text" name="bank_business_date_established" class="form-control daterange-single" value="{{ $application->bank_business_date_established }}" placeholder="Date Established">
                </div>
            </div>
        </div>
        <!-- current ownership -->
        <div class="col-md-6">
            <div class="form-group">
                <label>Current Ownership Since:</label>
                <div class="input-group">
                    <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-calendar22"></i></span>
                    </span>
                    <input type="text" name="bank_business_current_ownership" class="form-control daterange-single" value="{{ $application->bank_business_preferred_email }}" placeholder="Current Ownership Since">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- type -->
            <div class="form-group">
                <label>Type:</label>
                <div class="col-lg-12">
                    <!-- solo proprietor -->
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="bank_business_type" data-fouc value="1" {{ $application->bank_business_type == 1 ? 'checked' : '' }}>
                            Solo Proprietor
                        </label>
                    </div>
                    <!-- corporation -->
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="bank_business_type" data-fouc value="2" {{ $application->bank_business_type == 2 ? 'checked' : '' }}>
                            Corporation
                        </label>
                    </div>

                    <!-- llc -->
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="bank_business_type" data-fouc value="3" {{ $application->bank_business_type == 3 ? 'checked' : '' }}>
                            LLC
                        </label>
                    </div>

                    <!-- partnership -->
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="bank_business_type" data-fouc value="4" {{ $application->bank_business_type == 4 ? 'checked' : '' }}>
                            Partnership
                        </label>
                    </div>

                    <!-- LLP -->
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="bank_business_type" data-fouc value="5" {{ $application->bank_business_type == 5 ? 'checked' : '' }}>
                            LLP
                        </label>
                    </div>
                    <!-- trust -->
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="bank_business_type" data-fouc value="6" {{ $application->bank_business_type == 6 ? 'checked' : '' }}>
                            Trust
                        </label>
                    </div>
                    <!-- Other -->
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-input-styled" name="bank_business_type" data-fouc value="7" {{ $application->bank_business_type == 7 ? 'checked' : '' }}>
                            Other
                        </label>
                    </div>
                </div>
                <input type="text" name="bank_business_type_other" class="form-control" value="{{ $application->bank_business_type_other }}" placeholder="other type">
            </div>
        </div>
    </div>
</fieldset>
