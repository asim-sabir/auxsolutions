<div class="row">
    <div class="col-md-12">
        <div class="card" style="border: none !important;box-shadow: none;!important;">
            <form class="wizard-form banking-steps-basic" data-fouc id="banking_form" action="{{ auth()->guard('admin')->check() ? route('admin.application.store') : route('panel.application.store') }}">
                <input type="hidden" name="id" value="{{ $application_no }}">
                <h6>Business Profile</h6>
                @include('application.bank.business_profile')

                <h6>Nature Of Business</h6>
                @include('application.bank.nature_of_business')
            </form>
        </div>
    </div>
</div>
