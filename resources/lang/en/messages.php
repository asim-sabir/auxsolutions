<?php

return [
    'success' => [
        'create' => ':attribute information saved successfully',
        'update' => ':attribute information updated successfully',
        'destroy' => ':attribute information deleted successfully',
        'saved' => ':attribute information saved successfully',
        'not_found' => ':name not found',
        'upload' => ':name file successfully uploaded!',
        'sent' => 'We have e-mailed your :attribute',
    ],
    'error' => [
        'not_found' => 'No :attribute found',
        'general' => 'Oops!something is wrong. Try later',
        'select' => 'Nothing is selected',
        'permission' => 'You don\'t have permission to perform this action.',
        'exists' => 'The selected :attribute is already :val',
        'min' => 'The :attribute must be at least :min.',
        'changed' => 'You\'ve already changed the :attribute!',
        'pending' => ':action is pending at :attribute',
        'add_quote' => 'Please add Quotation first!',
    ]
];