<?php

namespace App\Http\Controllers\Customer;

use App\HelperModules\FileModules;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class AttachmentController extends Controller
{

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function agreement(Request $request)
    {
        $user = User::basicInfo()->findLinkToken($request->token)->first();
        if (!$user)
            return abort(404);

        return view('attachment.index', compact('user'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function upload(Request $request)
    {
        $this->validate($request, [
            'link_token' => 'required|exists:users,link_to_file_upload',
            'fileToUpload' => 'required|mimes:doc,docx,pdf'
        ]);
        $user = User::basicInfo()->findLinkToken($request->link_token)->first();
        $file_name = FileModules::FileUpload($request->file('fileToUpload'), 'confidential', $user->id);
        if (!$file_name)
            return redirect()->back()->with('error', Lang::get('messages.error.general'));

        $user->link_to_file_upload = null;
        $user->uploaded_partner_agreement_file = $file_name;
        $user->save();

        return redirect('/')->with('success', Lang::get('messages.success.upload', ['name' => 'Agreement']));
    }
}
