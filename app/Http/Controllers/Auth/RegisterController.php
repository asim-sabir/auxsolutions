<?php

namespace App\Http\Controllers\Auth;

use App\HelperModules\HelperModule;
use App\Models\User;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:150'],
            'email' => ['required', 'string', 'email', 'max:150', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone_no' => ['required', 'max:150'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'id' => '',
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone_no' => $data['phone_no'],
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

//        $this->guard()->login($user);
        $this->sentWelcomeEmail($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->with('success', 'Welcome to Aux. We have e-mailed you!');
    }

    /**
     * @param $user
     * @throws Exception
     */
    protected function sentWelcomeEmail($user)
    {
        /*$user->link_to_file_upload = HelperModule::randomBytes(35);
        $user->save();*/
//        $link = route('customer.partner.agreement', ['token' => $user->link_to_file_upload]);
        $link = route('panel.application');
        Mail::send('mails.agent-welcome-email', ['link' => $link, 'data' => $user], function ($message) use ($user) {
            $message->subject('Welcome to your new Back office portal by AuxPAY Solution')
                ->from(config('mail.from.address'), 'AUXPAY')
                ->to($user->email)->subject('Welcome!');
            /*$message->attach(public_path('agreement/sales_partner.docx'), [
                'as' => 'Sale partner Agreement.docx',
                'mime' => 'application/docx'
            ]);*/
        });
    }
}
