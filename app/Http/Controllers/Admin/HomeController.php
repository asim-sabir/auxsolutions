<?php

namespace App\Http\Controllers\Admin;

use App\HelperModules\Constants;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * @param Request $request
     * @return Factory|View
     */
    public function home(Request $request)
    {
        $data = User::get();
        $customers = $data->where('user_type', Constants::CUSTOMER)->count();
        $merchants = $data->where('user_type', Constants::MERCHANT)->count();
        return view('admin.home', compact('customers', 'merchants'));
    }
}
