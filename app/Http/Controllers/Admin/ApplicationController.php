<?php

namespace App\Http\Controllers\Admin;

use App\HelperModules\Constants;
use App\Models\Application\Application;
use App\Repositories\ApplicationRepository;
use Illuminate\Contracts\View\Factory;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class ApplicationController extends Controller
{
    protected $repo_obj;

    /**
     * ApplicationController constructor.
     */
    public function __construct()
    {
        $this->repo_obj = new ApplicationRepository();
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $applications = Application::with('agentInfo')->get();
        $pending = $applications->where('application_status', Constants::PENDING);
        $complete = $applications->where('application_status', Constants::COMPLETE);
        return view('admin.application.index', compact('applications', 'pending', 'complete'));
    }

    /**
     * @param Request $request
     * @return
     * @throws ValidationException
     */
    public function storeQuestionnaireForm(Request $request)
    {
        return $this->repo_obj->storeQuestionnaireForm($request);
    }

    /**
     * @param Request $request
     * @return Collection
     */
    public function store(Request $request)
    {
        return $this->repo_obj->store($request);
    }

    /**
     * @param Application $application
     * @return Factory|View
     */
    public function edit(Application $application)
    {
        $application_no = $application->id;
        return view('application.edit', compact('application', 'application_no'));
    }

    /**
     * @param Application $application
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Application $application)
    {
        return $this->repo_obj->destroy($application);
    }
}
