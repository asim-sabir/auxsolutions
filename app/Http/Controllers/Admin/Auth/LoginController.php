<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\View\View;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function login()
    {
        if (Auth::guard('admin')->check()) {
            return redirect(route('admin.home'));
        }
        return view('auth.admin.login');
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function loginPost(Request $request)
    {
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
        return redirect()->back()->withErrors(Lang::get('auth.failed'));

    }

    /**
     * @param $request
     * @return mixed
     */
    protected function attemptLogin($request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        return redirect(route('admin.home'));
    }

    /**
     * @return mixed
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function logout(Request $request)
    {
        $this->guard('admin')->logout();
        return redirect(route('admin.login'));
    }


}
