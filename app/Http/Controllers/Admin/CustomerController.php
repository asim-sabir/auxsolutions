<?php

namespace App\Http\Controllers\Admin;

use App\HelperModules\Constants;
use App\Models\User;
use Exception;
use Illuminate\Contracts\View\Factory;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Lang;
use Illuminate\View\View;

class CustomerController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        $customers = User::where('user_type', Constants::CUSTOMER)->get();
        return view('admin.customer.index', compact('customers'));
    }

    /**
     * @param User $user
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(User $user)
    {
        if ($user->delete())
            return redirect()->back()->withSuccess(Lang::get('messages.success.destroy', ['attribute' => 'Customer']));

        return redirect()->back()->withErrors(Lang::get('messages.error.general'));
    }
}
