<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Repositories\ApplicationRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class ApplicationController extends Controller
{
    protected $repo_obj;

    /**
     * ApplicationController constructor.
     */
    public function __construct()
    {
        $this->repo_obj = new ApplicationRepository();
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        return $this->repo_obj->index();
    }

    /**
     * @param $application_no
     * @return Factory|View
     */
    public function create($application_no)
    {
        return $this->repo_obj->create($application_no);
    }

    /**
     * @param Request $request
     * @return Collection
     */
    public function store(Request $request)
    {
        return $this->repo_obj->store($request);
    }

    /**
     * @param Request $request
     * @return Collection
     */
    public function autoStoreQuestionnaireForm(Request $request)
    {
        return $this->repo_obj->autoStoreQuestionnaireForm($request);
    }

    /**
     * @param Request $request
     * @return
     * @throws ValidationException
     */
    public function storeQuestionnaireForm(Request $request)
    {
        return $this->repo_obj->storeQuestionnaireForm($request);
    }

    /**
     * @param Request $request
     * @return Collection
     */
    public function share(Request $request)
    {
        return $this->repo_obj->share($request);
    }

    /**
     * @param Request $request
     * @return Collection
     */
    public function shareWithMerchant(Request $request)
    {
        return $this->repo_obj->shareWithMerchant($request);
    }

    /**
     * @param Request $request
     * @return Factory|RedirectResponse|View
     */
    public function agentApplication(Request $request)
    {
        return $this->repo_obj->agentApplication($request);
    }
}
