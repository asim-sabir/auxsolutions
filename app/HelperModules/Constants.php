<?php

namespace App\HelperModules;

/* @author <khakan.ali@alpharages.com> */
class Constants
{
    const CUSTOMER = 2;
    const MERCHANT = 1;
    const QUESTIONNAIRE_FORM = 1;
    const PENDING = 1;
    const COMPLETE = 2;
}
