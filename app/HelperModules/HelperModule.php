<?php

namespace App\HelperModules;

/* @author <khakan.ali@alpharages.com> */
class HelperModule
{

    /**
     * @param $type
     * @param $message
     * @param $data
     * @return \Illuminate\Support\Collection
     */
    public static function jsonResponse($type, $message = false, $data = null)
    {
        $response['isResponse'] = $type;
        if ($message)
            $response['message'] = $message;
        if ($data)
            $response['data'] = $data;

        return collect($response);
    }

    /**
     * @param $prefix
     * @return string
     */
    public static function CustomID($prefix)
    {
        $current_date = DateTimeModule::CurrentDateTime()->format('Ymd');
        $id = $current_date . $prefix . self::RandNo();
        return $id;
    }


    /**
     * @param int $min
     * @param int $max
     * @return int
     */
    public static function randNo($min = 10, $max = 1000)
    {
        return mt_rand($min, $max);
    }

    /**
     * @param int $length
     * @return string
     * @throws \Exception
     */
    public static function randomBytes($length = 70)
    {
        return bin2hex(random_bytes($length));
    }
}