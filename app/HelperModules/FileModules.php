<?php

namespace App\HelperModules;

use Illuminate\Support\Facades\Storage;

/* @author <khakan.ali@alpharages.com> */
class FileModules
{

    /**
     * @param $file
     * @param $path
     * @param $folderName
     * @param string $base_folder
     * @return mixed
     */
    public static function FileUpload($file, $path, $folderName, $base_folder = 'uploads/')
    {
        $destination_folder = base_path('public/' . $base_folder . $path . '/');
        self::make_dir($destination_folder, 0777, true);
        $image_name = self::RandomFileName($file, $folderName);
        $file->move($destination_folder, $image_name);
        return $image_name;
    }

    public static function FileUploadToStorage($file, $path, $driver)
    {
        $storage = Storage::disk($driver)->put($path, $file);
        return $storage;
    }

    /** -- Make directory --
     * @param $path
     * @param $permission
     * @param bool $recursive
     * @return bool
     */
    public static function make_dir($path, $permission, $recursive = false)
    {
        if (file_exists($path))
            return true;
        return mkdir($path, $permission, $recursive);
    }

    /**
     * @param $file
     * @return string
     */
    public static function RandomFileName($file, $name)
    {
        $type = $file->guessExtension();
        return $name . '-' . DateTimeModule::TimeInMilliSec() . '.' . $type;
    }

    /**
     * @param $path
     * @return string
     */
    public static function GetFilePath($path)
    {
        return asset($path);
    }

    /**
     * @param $file
     * @return string
     */
    public static function CovertToBase64($file)
    {
        return base64_encode(file_get_contents($file));
    }
}