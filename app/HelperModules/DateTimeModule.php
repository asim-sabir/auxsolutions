<?php

namespace App\HelperModules;

use Carbon\Carbon;
use DateTime;

/* @author <khakan.ali@alpharages.com> */
class DateTimeModule
{
    /**
     * @param $time
     * @param string $format
     * @return string
     */
    public static function TimeFormat($time, $format = 'Y-m-d G:i:s')
    {
        return Carbon::createFromFormat('Y-m-d G:i:s', self::CarbonObject($time)->toDateTimeString())->format($format);
    }


    /**
     * @param string $format
     * @return string
     */
    public static function CurrentTime($format = 'Y-m-d G:i:s')
    {
        return self::TimeFormat(self::CurrentDateTime(), $format);
    }

    /**
     * @return DateTimeModule|Carbon
     */
    public static function CurrentDateTime()
    {
        return Carbon::now();
    }

    /**
     * @param $data
     * @return DateTimeModule|Carbon
     */
    public static function CarbonObject($data)
    {
        return Carbon::parse($data);
    }

    /**
     * @param $second_date
     * @return mixed
     */
    public static function DiffForHumans($second_date)
    {
        $current_date = self::CurrentDateTime();
        $second_date = self::CarbonObject($second_date);
        return $second_date->diffForHumans($current_date, true);
    }

    /**
     * @param $first_date
     * @param $second_date
     * @param string $type
     * @return mixed
     */
    public static function DiffForHumansBetweenDates($first_date, $second_date, $type = 'human')
    {
        $first_date = $first_date ? self::CarbonObject($first_date) : self::CurrentDateTime();
        $second_date = $second_date ? self::CarbonObject($second_date) : self::CurrentDateTime();
        switch ($type) {
            case 'human':
                $date = $second_date->diffForHumans($first_date, true);
                break;
            case 'hours':
                $date = $second_date->diffInHours($first_date);
                break;
            case 'minutes':
                $date = $second_date->diffInMinutes($first_date);
                break;
            case 'seconds':
                $date = $second_date->diffInSeconds($first_date);
                break;
            default:
                $date = $second_date->diffForHumans($first_date, true);
                break;
        }
        return $date;
    }

    /**
     * @param $date
     * @return false|int
     */
    public static function StrToTime($date = null)
    {
        if (!$date)
            $date = Carbon::now();
        return strtotime($date);
    }

    /**
     * @return float
     */
    public static function TimeInMilliSec()
    {
        return round(microtime(true) * 1000);
    }

    /**
     * @param $seconds
     * @return string
     */
    public static function SecToHR($seconds)
    {
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds / 60) % 60);
        $seconds = $seconds % 60;
        $str = null;
        if ($hours > 0)
            $str = "$hours Hours,";
        if ($minutes > 0)
            $str .= " $minutes Minutes,";
        if ($seconds > 0)
            $str .= " $seconds Seconds";
        return rtrim($str, ',');
    }

    /**
     * @param $seconds
     * @return string
     */
    public static function secToHRWithOutFormat($seconds)
    {
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds / 60) % 60);
        $seconds = $seconds % 60;
        return "$hours:$minutes:$seconds";
    }

    /**
     * @param $seconds
     * @return float|int
     */
    public static function MinToSec($seconds)
    {
        return $seconds * 60;
    }

    /**
     * @return float|int
     */
    public static function CurrentDateSec()
    {
        return self::CurrentDateTime()->secondsSinceMidnight();
    }

    /**
     * @param $date
     * @param string $format
     * @return false|string
     */
    public static function GMTDate($date, $format = 'Y-m-d G:i:s')
    {
        return gmdate($format, $date);
    }

    /**
     * @param $date
     * @param $time_zone
     * @return DateTime
     */
    public static function CovertToDefault($date, $time_zone = null)
    {
        if (is_null($time_zone)) {
            $user_time_zone = HelperModule::TimeZone();
            if ($user_time_zone)
                $time_zone = $user_time_zone['time-zone'][0];
        }
        return Carbon::parse($date, $time_zone)->setTimezone(config('app.timezone'));
    }

    /**
     * @param $date
     * @param $time_zone
     * @return DateTime
     */
    public static function CovertDefaultToOther($date, $time_zone)
    {
        $date = $date->toDateTime()->format('Y-m-d H:i:s');
        return Carbon::parse($date, config('app.timezone'))->setTimezone($time_zone);
    }

    /**
     * @param $date
     * @param $time_zone
     * @return DateTime
     */
    public static function CovertDefaultToOtherWithCarbon($date, $time_zone = null)
    {
        if (is_null($time_zone))
            $time_zone = HelperModule::TimeZone()['time-zone'][0];
        return Carbon::parse($date, config('app.timezone'))->setTimezone($time_zone);
    }

    /**
     * @param $date
     * @return bool
     */
    public static function CheckDateIsBetweenLastTwoDays($date)
    {
        if ($date >= self::CurrentDateTime()->subDays(2)->startOfDay())
            return true;

        return false;
    }

    /**
     * @param $mil
     * @param string $format
     * @return false|string
     */
    public static function MiliSecToDate($mil, $format = 'Y-m-d H:i:s')
    {
        $seconds = $mil / 1000;
        return date($format, $seconds);
    }

    /**
     * @param $months
     * @return Carbon
     */
    public static function SubMonths($months)
    {
        return self::CurrentDateTime()->subMonths($months);
    }

    /**
     * @param $seconds
     * @return float
     */
    public static function convertSecToMinutes($seconds)
    {
        return round(($seconds / 60) % 60, 2);
    }

    /**
     * @param $seconds
     * @return float
     */
    public static function convertSecToHours($seconds)
    {
        return round($seconds / 3600, 2);
    }
}