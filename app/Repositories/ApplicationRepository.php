<?php

namespace App\Repositories;

use App\HelperModules\Constants;
use App\HelperModules\FileModules;
use App\HelperModules\HelperModule;
use App\Models\Application\Application;
use App\Models\User;
use App\Notifications\ShareApplicationEmail;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

/* @author <khakan.ali@alpharages.com> */
class ApplicationRepository
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        $new_application = null;
        $applications = Application::basicInfo();
        if (auth()->user()->user_type == Constants::CUSTOMER) {
            $applications = $applications->agentApplications([auth()->id()])->paginate(config('app.paginate'));
            return view('application.index', compact('new_application', 'applications'));
        }

        $applications = $applications->where('user_id', auth()->id())->paginate(config('app.paginate'));
        $new_application = Application::count() + 1;
        $new_application = HelperModule::CustomID('AP' . $new_application);
        return view('application.index', compact('new_application', 'applications'));
    }

    /**
     * @param $application_no
     * @return Factory|View
     */
    public function create($application_no)
    {
        $application = Application::firstOrCreate(['id' => $application_no], ['user_id' => auth()->id()]);
        return view('panel.application.create', compact('application_no', 'application'));
    }

    /**
     * @param Request $request
     * @return Collection
     */
    public function store(Request $request)
    {
        try {
            $request->request->add(['user_id' => Auth::id()]);
            $request->request->add(['fee_schedule_status' => $request->has('fee_schedule_status') ? $request->fee_schedule_status : 0]);
            $application = Application::find($request->id);
            if ($application) {
                $request = $request->except(['id', 'user_id']);
                $application->update($request);
            } else {
                Application::create($request->all());
            }
            return HelperModule::jsonResponse(true, Lang::get('messages.success.saved', ['attribute' => 'Application']));
        } catch (Exception $e) {
            return HelperModule::jsonResponse(false, $e->getMessage());
        }

    }

    /**
     * @param Request $request
     * @return Collection
     */
    public function autoStoreQuestionnaireForm(Request $request)
    {
        try {
            $request->request->add(['user_id' => Auth::id()]);
            Application::updateOrCreate(['id' => $request->id], $request->all());

            return HelperModule::jsonResponse(true, Lang::get('messages.success.saved', ['attribute' => 'Initial']));
        } catch (Exception $e) {
            return HelperModule::jsonResponse(false, $e->getMessage());
        }

    }

    /**
     * @param Request $request
     * @return
     */
    public function storeQuestionnaireForm(Request $request)
    {
        $application_obj = new Application();
        $this->validate($request, $application_obj->rules(), $application_obj->messages());

        $request->request->add(['user_id' => Auth::id()]);
        $path = 'confidential/' . auth()->id();
        $file_name = FileModules::FileUpload($request->file('licence'), $path, 'licence-' . Auth::id());
        $request->request->add(['merchant_licence' => $file_name]);
        $application = Application::updateOrCreate(['id' => $request->id], $request->all());
        if (!auth()->guard('admin')->check()) {
            $link = route('admin.login');
            // notify admin
            Mail::send('mails.application_mail_to_admin', ['link' => $link, 'merchant_name' => auth()->user()->first_name, 'data' => $application], function ($message) use ($application) {
                $message->subject('New Application #:' . $application->id);
                $message->from(config('mail.from.address'), 'AUX');
                $message->to('team@auxpay.net');
            });
        }
        return redirect()->back()->withSuccess(Lang::get('messages.success.create', ['attribute' => 'Initial']));
    }

    /**
     * @param Request $request
     * @return Collection
     */
    public function share(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),
                ['email' => 'required', 'application_id' => 'required'],
                ['email.required' => Lang::get('validation.required', ['attribute' => 'email'])]);
            if ($validator->fails())
                return HelperModule::jsonResponse(false, $validator->errors()->first());
            $application = Application::find($request->application_id);
            if (!$application)
                return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'Application']));
            //check if email already exist
            $user = User::basicInfo()->byEmail($request->email)->first();
            if (!$user) {
                $request->request->add(['new_user' => 1]);
                $user = User::create(['id' => '', 'first_name' => 'customer', 'email' => $request->email, 'password' => bcrypt($request->application_id), 'user_type' => Constants::CUSTOMER]);
            }
            if (!$user->ShareApplications->where('application_id', $request->application_id)->first())
                $user->ShareApplications()->create(['id' => '', 'application_id' => $request->application_id]);
            // notify user
            $request->request->add(['view' => 'share-application']);
            $user->notify(new ShareApplicationEmail($request));

            return HelperModule::jsonResponse(true, Lang::get('messages.success.sent', ['attribute' => 'application']));
        } catch (Exception $e) {
            return HelperModule::jsonResponse(false, $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return Collection
     */
    public function shareWithMerchant(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), ['application_id' => 'required']);
            if ($validator->fails())
                return HelperModule::jsonResponse(false, $validator->errors()->first());
            $application = Application::find($request->application_id);
            if (!$application)
                return HelperModule::jsonResponse(false, Lang::get('messages.error.not_found', ['attribute' => 'Application']));
            $agent = $application->agentInfo;
            $request->request->add(['customer_name' => auth()->user()->first_name]);
            // notify user
            $request->request->add(['view' => 'application-notification']);
            $agent->notify(new ShareApplicationEmail($request));

            return HelperModule::jsonResponse(true, Lang::get('messages.success.sent', ['attribute' => 'application']));
        } catch (Exception $e) {
            return HelperModule::jsonResponse(false, $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return Factory|RedirectResponse|View
     */
    public function agentApplication(Request $request)
    {
        $application = Application::basicInfo()->agentApplications([auth()->id()])->first();
        if (!$application)
            return redirect()->back()->withErrors(Lang::get('messages.error.not_found', ['attribute' => 'Application']));
        $application_no = $application->id;
        return view('application.edit', compact('application_no', 'application'));
    }

    /**
     * @param Application $application
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Application $application)
    {
        if($application->delete())
            return redirect()->back()->withSuccess(Lang::get('messages.success.destroy', ['attribute' => 'Application']));

        return redirect()->back()->withErrors(Lang::get('messages.error.general'));
    }
}