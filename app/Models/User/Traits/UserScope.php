<?php

namespace App\Models\User\Traits;


trait UserScope
{
    /**
     * @param $query
     * @param $token
     * @return mixed
     */
    public function scopeFindLinkToken($query, $token)
    {
        return $query->where('link_to_file_upload', $token);
    }

    /**
     * @param $query
     * @param $email
     * @return mixed
     */
    public function scopeByEmail($query, $email)
    {
        return $query->where('email', $email);
    }
}
