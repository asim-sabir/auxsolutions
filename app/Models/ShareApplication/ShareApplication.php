<?php

namespace App\Models\ShareApplication;

use App\HelperModules\HelperModule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShareApplication extends Model
{
    use SoftDeletes;
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'application_id',
        'user_id',
        'status'
    ];

    protected $dates = ['created_at'];

    /**
     * @param $val
     */
    public function setIdAttribute($val)
    {
        $last_id = $this->count() + 1;
        $this->attributes['id'] = HelperModule::CustomID('APS' . $last_id);
    }

    /**
     * @param array $select
     * @return mixed
     */
    public static function basicInfo($select = ['*'])
    {
        return self::select($select)->orderBy('created_at', 'desc');
    }
}
