<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/* @author <khakan.ali@alpharages.com> */
class Admin extends Authenticatable

{
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password'];
}
