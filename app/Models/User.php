<?php

namespace App\Models;

use App\HelperModules\HelperModule;
use App\Models\Application\Application;
use App\Models\ShareApplication\ShareApplication;
use App\Models\User\Traits\UserScope;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, UserScope;

    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'email',
        'password',
        'phone_no',
        'uploaded_partner_agreement_file',
        'link_to_file_upload',
        'user_type',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param $val
     */
    public function setIdAttribute($val)
    {
        $last_id = $this->count() + 1;
        $this->attributes['id'] = HelperModule::CustomID('PTR' . $last_id);
    }


    /**
     * @param array $select
     * @return mixed
     */
    public static function basicInfo($select = ['*'])
    {
        return self::select($select);
    }

    /**
     * @return HasMany
     */
    public function Applications()
    {
        return $this->hasMany(Application::class, 'user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function ShareApplications()
    {
        return $this->hasMany(ShareApplication::class, 'user_id', 'id');
    }
}
