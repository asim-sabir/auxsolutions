<?php

namespace App\Models\Application;

use App\HelperModules\DateTimeModule;
use App\Models\Application\Traits\ApplicationValidation;
use App\Models\ShareApplication\ShareApplication;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use SoftDeletes, ApplicationValidation;
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'questionnaire_business_name',
        'questionnaire_merchant_dba',
        'questionnaire_dba_address',
        'questionnaire_dba_city',
        'questionnaire_dba_state',
        'questionnaire_dba_zip',
        'questionnaire_business_phone_no',
        'questionnaire_total_business_months',
        'questionnaire_email',
        'questionnaire_annual_sales',
        'questionnaire_tax_id',
        'questionnaire_ownership_type',
        'questionnaire_legal_address',
        'questionnaire_legal_city',
        'questionnaire_legal_state',
        'questionnaire_legal_zip',
        'questionnaire_owner_1_name',
        'questionnaire_owner_1_address',
        'questionnaire_owner_1_state',
        'questionnaire_owner_1_city',
        'questionnaire_owner_1_zip',
        'questionnaire_owner_1_ownership_percentage',
        'questionnaire_owner_1_birthday',
        'questionnaire_owner_1_social_security',
        'questionnaire_owner_2_name',
        'questionnaire_owner_2_address',
        'questionnaire_owner_2_state',
        'questionnaire_owner_2_city',
        'questionnaire_owner_2_zip',
        'questionnaire_owner_2_ownership_percentage',
        'questionnaire_owner_2_birthday',
        'questionnaire_owner_2_social_security',
        'questionnaire_has_previous_application',
        'merchant_licence',
        'corporate_name',
        'corporate_address',
        'corporate_city',
        'corporate_state',
        'corporate_zip',
        'corporate_phone_no',
        'corporate_fax_no',
        'corporate_federal_tex_id',
        'corporate_contact_person',
        'doing_business_as',
        'business_address',
        'business_city',
        'business_state',
        'business_zip',
        'business_phone_no_1',
        'business_phone_no_2',
        'business_email',
        'type_of_entity',
        'firm_type',
        'type_of_goods_sold',
        'web_address',
        'length_of_ownership_yrs',
        'length_of_ownership_mos',
        'length_of_time_at_location_yrs',
        'length_of_time_at_location_mos',
        'year_business_established',
        'is_licensed_medical_cannabis',
        'is_licensed_medical_cannabis_detail',
        'date_of_original_licence_granted',
        'date_of_original_licence_granted_detail',
        'license_no',
        'license_no_detail',
        'license_expiration_date',
        'license_expiration_date_detail',
        'licence_terminated',
        'licence_terminated_detail',
        'name_1',
        'title_1',
        'residential_address_1',
        'residential_city_1',
        'residential_state_1',
        'residential_zip_1',
        'driver_license_number_1',
        'ssn_1',
        'equity_ownership_1',
        'time_at_residence_1_yrs',
        'time_at_residence_1_mos',
        'residence_type_own_1',
        'residence_type_rent_1',
//        'date_of_birth_1',
        'residence_telephone_1',
        'name_2',
        'title_2',
        'residential_address_2',
        'residential_city_2',
        'residential_state_2',
        'residential_zip_2',
        'driver_license_number_2',
        'ssn_2',
        'equity_ownership_2',
        'time_at_residence_2_yrs',
        'time_at_residence_2_mos',
        'residence_type_own_2',
        'residence_type_rent_2',
//        'date_of_birth_2',
        'residence_telephone_2',
        'bank_reference',
        'bank_reference_account_no',
        'bank_reference_telephone_no',
        'bank_reference_contact',
        'trade_reference_1',
        'trade_reference_1_account_no',
        'trade_reference_1_telephone_no',
        'trade_reference_1_contact',
        'trade_reference_2',
        'trade_reference_2_account_no',
        'trade_reference_2_telephone_no',
        'trade_reference_2_contact',
        'trade_reference_3',
        'trade_reference_3_account_no',
        'trade_reference_3_telephone_no',
        'trade_reference_3_contact',
        'form_w9_name',
        'form_w9_business_name',
        'form_w9_address',
        'form_w9_city',
        'form_w9_state',
        'form_w9_zip_code',
        'form_w9_requester_name',
        'form_w9_requester_address',
        'form_w9_account_no_list',
        'social_security_number',
        'employer_identification_number',
        'fee_schedule_status',
        'one_time_equipment_set_up',
        'one_time_equipment_set_up_amount',
        'additional_merchant_terminal_amount',
        'sale_volume_fee',
        'sale_volume_fee_amount',
        'per_transaction_amount',
        'charge_back',
        'charge_back_amount',
        'per_returned_ach_item_amount',
        'limit_charge_back',
        'limit_charge_back_amount',
        'reserve_account',
        'reserve_account_amount',
        'gross_transaction_volume',
        'bounding_amount',
        'bounding_amount_amount',
        // bank info
        'bank_applicant_legal_name',
        'bank_applicant_title',
        'bank_applicant_email',
        'bank_applicant_phone_no',
        'bank_business_name',
        'bank_business_ein',
        'bank_business_registered_ficitious_name',
        'bank_business_corporate_address',
        'bank_business_phone_no',
        'bank_business_preferred_email',
        'bank_business_date_established',
        'bank_business_current_ownership',
        'bank_business_type',
        'bank_business_type_other',
        'bank_business_products',
        'bank_business_operations',
        'bank_business_current_licenses',
        'bank_business_monthly_sales',
        'questionnaire_form_status',
        'application_status'
    ];

    protected $dates = ['created_at'];
    /**
     * @param $val
     */
    /*public function setIdAttribute($val)
    {
        $last_id = $this->count() + 1;
        $this->attributes['id'] = HelperModule::CustomID('PTR' . $last_id);
    }*/

    /**
     * @param $val
     * @return mixed
     */
    public function setBankBusinessDateEstablishedAttribute($val)
    {
        if (!$val)
            $this->attributes['bank_business_date_established'] = DateTimeModule::CarbonObject($val)->toDateString();
    }

    /**
     * @param $val
     * @return mixed
     */
    public function setBankBusinessCurrentOwnershipAttribute($val)
    {
        if (!$val)
            $this->attributes['bank_business_current_ownership'] = DateTimeModule::CarbonObject($val)->toDateString();
    }

    /**
     * @param $query
     * @param array $id
     * @return mixed
     */
    public function scopeAgentApplications($query, array $id)
    {
        return $query->whereHas('SharedApplications', function ($query) use ($id) {
            $query->whereIn('user_id', $id);
        });
    }

    /**
     * @param array $select
     * @return mixed
     */
    public static function basicInfo($select = ['*'])
    {
        return self::select($select)->orderBy('created_at', 'desc');
    }

    /**
     * @return HasMany
     */
    public function SharedApplications()
    {
        return $this->hasMany(ShareApplication::class, 'application_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function agentInfo()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
