<?php

namespace App\Models\Application\Traits;


use Illuminate\Support\Facades\Lang;

trait ApplicationValidation
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'questionnaire_business_name' => 'required',
            'questionnaire_merchant_dba' => 'required',
            'questionnaire_dba_address' => 'required',
            'questionnaire_dba_city' => 'required',
            'questionnaire_dba_state' => 'required',
            'questionnaire_dba_zip' => 'required',
            'questionnaire_business_phone_no' => 'required',
            'questionnaire_total_business_months' => 'required',
            'questionnaire_email' => 'required',
            'questionnaire_annual_sales' => 'required',
            'questionnaire_tax_id' => 'required',
            'questionnaire_ownership_type' => 'required',
            'questionnaire_legal_address' => 'required',
            'questionnaire_legal_city' => 'required',
            'questionnaire_legal_state' => 'required',
            'questionnaire_legal_zip' => 'required',
            'questionnaire_owner_1_name' => 'required',
            'questionnaire_owner_1_address' => 'required',
            'questionnaire_owner_1_city' => 'required',
            'questionnaire_owner_1_state' => 'required',
            'questionnaire_owner_1_zip' => 'required',
            'questionnaire_owner_1_ownership_percentage' => 'required',
            'questionnaire_owner_1_birthday' => 'required',
            'questionnaire_owner_1_social_security' => 'required',
            'licence' => 'required|mimes:jpg,jpeg,png'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'questionnaire_business_name.required' => Lang::get('validation.required', ['attribute' => 'Legal Name Of Business']),
            'questionnaire_merchant_dba.required' => Lang::get('validation.required', ['attribute' => 'Merchant DBA']),
            'questionnaire_dba_address.required' => Lang::get('validation.required', ['attribute' => 'Legal Name Of Business']),
            'DBA address.required' => Lang::get('validation.required', ['attribute' => 'DBA address']),
            'questionnaire_dba_city.required' => Lang::get('validation.required', ['attribute' => 'DBA City']),
            'questionnaire_dba_state.required' => Lang::get('validation.required', ['attribute' => 'DBA Sate']),
            'questionnaire_dba_zip.required' => Lang::get('validation.required', ['attribute' => 'DBA Zip']),
            'questionnaire_business_phone_no.required' => Lang::get('validation.required', ['attribute' => 'Business Tel No']),
            'questionnaire_total_business_months.required' => Lang::get('validation.required', ['attribute' => 'Total Months in Business']),
            'questionnaire_email.required' => Lang::get('validation.required', ['attribute' => 'Merchant Email']),
            'questionnaire_annual_sales.required' => Lang::get('validation.required', ['attribute' => 'Estimated Annual Sales']),
            'questionnaire_tax_id.required' => Lang::get('validation.required', ['attribute' => 'Federal Tax ID']),
            'questionnaire_ownership_type.required' => Lang::get('validation.required', ['attribute' => 'Type of Ownership']),
            'questionnaire_legal_address.required' => Lang::get('validation.required', ['attribute' => 'Legal Address']),
            'questionnaire_legal_city.required' => Lang::get('validation.required', ['attribute' => 'City']),
            'questionnaire_legal_state.required' => Lang::get('validation.required', ['attribute' => 'State']),
            'questionnaire_legal_zip.required' => Lang::get('validation.required', ['attribute' => 'Zip']),
            'questionnaire_owner_1_name.required' => Lang::get('validation.required', ['attribute' => 'Full Name']),
            'questionnaire_owner_1_address.required' => Lang::get('validation.required', ['attribute' => 'Residential Address']),
            'questionnaire_owner_1_city.required' => Lang::get('validation.required', ['attribute' => 'City']),
            'questionnaire_owner_1_state.required' => Lang::get('validation.required', ['attribute' => 'State']),
            'questionnaire_owner_1_zip.required' => Lang::get('validation.required', ['attribute' => 'Zip']),
            'questionnaire_owner_1_ownership_percentage.required' => Lang::get('validation.required', ['attribute' => 'Ownership']),
            'questionnaire_owner_1_birthday.required' => Lang::get('validation.required', ['attribute' => 'Birthday']),
            'questionnaire_owner_1_social_security.required' => Lang::get('validation.required', ['attribute' => 'Social Security'])
        ];
    }
}
